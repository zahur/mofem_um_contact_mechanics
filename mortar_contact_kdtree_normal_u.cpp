/* This file is part of MoFEM.
 * \ingroup nonlinear_elastic_elem

 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

 #include <BasicFiniteElements.hpp>
using namespace MoFEM;

#include <boost/program_options.hpp>
using namespace std;
namespace po = boost::program_options;
#include <Hooke.hpp>
using namespace boost::numeric;

#include "clipper.hpp"
using namespace ClipperLib;

#include "moab/AdaptiveKDTree.hpp"
#include "ContactSearchKdTree.hpp"
#include <ContactProblemKdTree.hpp>
#include <ContactProblemKdTreeNormalU.hpp>

static char help[] =
  "-my_block_config set block data\n"
  "\n";

struct BlockOptionData {
  int oRder;
  double yOung;
  double pOisson;
  double initTemp;
  BlockOptionData():
    oRder(-1),
    yOung(-1),
    pOisson(-2),
    initTemp(0) {}
};


int main(int argc, char *argv[]) {
  PetscInitialize(&argc,&argv,(char *)0,help);
  moab::Core mb_instance;
  moab::Interface& moab = mb_instance;


  PetscBool flg_block_config,flg_file;
  char mesh_file_name[255];
  char block_config_file[255];
  PetscInt order = 2;
  PetscBool is_partitioned = PETSC_FALSE;

  ierr = PetscOptionsBegin(PETSC_COMM_WORLD,"","Elastic Config","none"); CHKERRQ(ierr);
  ierr = PetscOptionsString(
    "-my_file",
    "mesh file name","",
    "mesh.h5m",mesh_file_name,255,&flg_file
  ); CHKERRQ(ierr);


  ierr = PetscOptionsInt(
    "-my_order",
    "default approximation order","",
    1,&order,PETSC_NULL
    ); CHKERRQ(ierr);


    ierr = PetscOptionsBool(
        "-my_is_partitioned",
        "set if mesh is partitioned (this result that each process keeps only part of the mes","",
        PETSC_FALSE,&is_partitioned,PETSC_NULL
      ); CHKERRQ(ierr);
      ierr = PetscOptionsString("-my_block_config",
        "elastic configure file name","",
        "block_conf.in",block_config_file,255,&flg_block_config
      ); CHKERRQ(ierr);
      ierr = PetscOptionsEnd(); CHKERRQ(ierr);

      //Reade parameters from line command
      if(flg_file != PETSC_TRUE) {
        SETERRQ(PETSC_COMM_SELF,1,"*** ERROR -my_file (MESH FILE NEEDED)");
      }


      ParallelComm* pcomm = ParallelComm::get_pcomm(&moab,MYPCOMM_INDEX);
      if(pcomm == NULL) pcomm =  new ParallelComm(&moab,PETSC_COMM_WORLD);

      if(is_partitioned == PETSC_TRUE) {
        //Read mesh to MOAB
        const char *option;
        option = "PARALLEL=BCAST_DELETE;"
          "PARALLEL_RESOLVE_SHARED_ENTS;"
          "PARTITION=PARALLEL_PARTITION;";
        rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
      } else {
        const char *option;
        option = "";
        rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
      }

      //Create MoFEM (Joseph) database
      MoFEM::Core core(moab);
      MoFEM::Interface& m_field = core;

      //print bcs
      MeshsetsManager *mmanager_ptr;
      ierr = m_field.query_interface(mmanager_ptr); CHKERRQ(ierr);
      ierr = mmanager_ptr->printDisplacementSet(); CHKERRQ(ierr);
      ierr = mmanager_ptr->printForceSet(); CHKERRQ(ierr);
      //print block sets with materials
      ierr = mmanager_ptr->printMaterialsSet(); CHKERRQ(ierr);

      // stl::bitset see for more details
      BitRefLevel bit_level0;
      bit_level0.set(0);
      ierr = m_field.seed_ref_level_3D(0,bit_level0); CHKERRQ(ierr);

      Range meshset_level0;
      ierr = m_field.get_entities_by_ref_level(bit_level0,BitRefLevel().set(),meshset_level0); CHKERRQ(ierr);
      PetscSynchronizedPrintf(PETSC_COMM_WORLD,"meshset_level0 %d\n",meshset_level0.size());
      PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);




      Range range_surf_master, range_surf_slave;
      for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,SIDESET,it)) {
        if(it->getName().compare(0,6,"Master") == 0) {
          // cout<<" HI from range_surf_master "<<endl;
          rval = m_field.get_moab().get_entities_by_type(it->meshset,MBTRI,range_surf_master,true); CHKERRQ_MOAB(rval);
        }
      }
      cout<<"range_surf_master = "<<range_surf_master.size()<<endl;

      for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,SIDESET,it)) {
        if(it->getName().compare(0,5,"Slave") == 0) {
          // cout<<" HI from range_surf_slave "<<endl;
          rval = m_field.get_moab().get_entities_by_type(it->meshset,MBTRI,range_surf_slave,true); CHKERRQ_MOAB(rval);
        }
      }
      cout<<"range_surf_slave = "<<range_surf_slave.size()<<endl;

      EntityHandle meshset_surf_slave, meshset_surf_master;
      rval = moab.create_meshset(MESHSET_SET,meshset_surf_slave); CHKERRQ_MOAB(rval);
      rval = moab.create_meshset(MESHSET_SET,meshset_surf_master); CHKERRQ_MOAB(rval);

      rval = moab.add_entities(meshset_surf_slave,range_surf_slave); CHKERRQ_MOAB(rval);
      rval = moab.add_entities(meshset_surf_master,range_surf_master); CHKERRQ_MOAB(rval);

      rval = moab.write_mesh("surf_slave.vtk", &meshset_surf_slave, 1);
      rval = moab.write_mesh("surf_master.vtk", &meshset_surf_master, 1);


      EntityHandle meshset_tri_slave, out_put_set, meshset_polygons;
      rval = moab.create_meshset(MESHSET_SET,meshset_tri_slave); CHKERRQ_MOAB(rval); ;
      rval = moab.create_meshset(MESHSET_SET,out_put_set); CHKERRQ_MOAB(rval);
      rval = moab.create_meshset(MESHSET_SET,meshset_polygons); CHKERRQ_MOAB(rval);

      //Define problem
      //Fields
      ierr = m_field.add_field("DISPLACEMENT",H1,AINSWORTH_LEGENDRE_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);

      //LAGMULT for normal displacement (rank 1)
      ierr = m_field.add_field("LAGMULT",H1,AINSWORTH_LEGENDRE_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);

      //Declare problem
      //add entitities (by tets) to the field
      ierr = m_field.add_ents_to_field_by_type(0,MBTET,"DISPLACEMENT"); CHKERRQ(ierr);
      ierr = m_field.add_ents_to_field_by_type(range_surf_slave,MBTRI,"LAGMULT"); CHKERRQ(ierr);

      //set app. order
      //see Hierarchic Finite Element Bases on Unstructured Tetrahedral Meshes (Mark Ainsworth & Joe Coyle)
      ierr = m_field.set_field_order(0,MBTET,"DISPLACEMENT",order); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBTRI,"DISPLACEMENT",order); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBEDGE,"DISPLACEMENT",order); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBVERTEX,"DISPLACEMENT",1); CHKERRQ(ierr);

      ierr = m_field.set_field_order(0,MBTRI,"LAGMULT",order); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBEDGE,"LAGMULT",order); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBVERTEX,"LAGMULT",1); CHKERRQ(ierr);

      // configure blocks by parsing config file
        // it allow to set approximation order for each block independently
        std::map<int,BlockOptionData> block_data;
        if(flg_block_config) {
          try {
            ifstream ini_file(block_config_file);
            //std::cerr << block_config_file << std::endl;
            po::variables_map vm;
            po::options_description config_file_options;
            for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,BLOCKSET,it)) {
              std::ostringstream str_order;
              str_order << "block_" << it->getMeshsetId() << ".displacement_order";
              config_file_options.add_options()
              (str_order.str().c_str(),po::value<int>(&block_data[it->getMeshsetId()].oRder)->default_value(order));
              std::ostringstream str_cond;
              str_cond << "block_" << it->getMeshsetId() << ".young_modulus";
              config_file_options.add_options()
              (str_cond.str().c_str(),po::value<double>(&block_data[it->getMeshsetId()].yOung)->default_value(-1));
              std::ostringstream str_capa;
              str_capa << "block_" << it->getMeshsetId() << ".poisson_ratio";
              config_file_options.add_options()
              (str_capa.str().c_str(),po::value<double>(&block_data[it->getMeshsetId()].pOisson)->default_value(-2));
              std::ostringstream str_init_temp;
              str_init_temp << "block_" << it->getMeshsetId() << ".initial_temperature";
              config_file_options.add_options()
              (str_init_temp.str().c_str(),po::value<double>(&block_data[it->getMeshsetId()].initTemp)->default_value(0));
            }
            po::parsed_options parsed = parse_config_file(ini_file,config_file_options,true);
            store(parsed,vm);
            po::notify(vm);
            for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,BLOCKSET,it)) {
              if(block_data[it->getMeshsetId()].oRder == -1) continue;
              if(block_data[it->getMeshsetId()].oRder == order) continue;
              PetscPrintf(PETSC_COMM_WORLD,"Set block %d order to %d\n",it->getMeshsetId(),block_data[it->getMeshsetId()].oRder);
              Range block_ents;
              rval = moab.get_entities_by_handle(it->getMeshset(),block_ents,true); CHKERRQ_MOAB(rval);
              Range ents_to_set_order;
              rval = moab.get_adjacencies(block_ents,3,false,ents_to_set_order,moab::Interface::UNION); CHKERRQ_MOAB(rval);
              ents_to_set_order = ents_to_set_order.subset_by_type(MBTET);
              rval = moab.get_adjacencies(block_ents,2,false,ents_to_set_order,moab::Interface::UNION); CHKERRQ_MOAB(rval);
              rval = moab.get_adjacencies(block_ents,1,false,ents_to_set_order,moab::Interface::UNION); CHKERRQ_MOAB(rval);
              ierr = m_field.synchronise_entities(ents_to_set_order); CHKERRQ(ierr);
              ierr = m_field.set_field_order(ents_to_set_order,"DISPLACEMENT",block_data[it->getMeshsetId()].oRder); CHKERRQ(ierr);
            }
            std::vector<std::string> additional_parameters;
            additional_parameters = collect_unrecognized(parsed.options,po::include_positional);
            for(std::vector<std::string>::iterator vit = additional_parameters.begin();
            vit!=additional_parameters.end();vit++) {
              ierr = PetscPrintf(PETSC_COMM_WORLD,"** WARNING Unrecognized option %s\n",vit->c_str()); CHKERRQ(ierr);
            }
          } catch (const std::exception& ex) {
            std::ostringstream ss;
            ss << ex.what() << std::endl;
            SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
          }
        }

      // Add elastic element
      boost::shared_ptr<Hooke<adouble> > hooke_adouble_ptr(new Hooke<adouble>());
      boost::shared_ptr<Hooke<double> > hooke_double_ptr(new Hooke<double>());
      NonlinearElasticElement elastic(m_field,2);
      ierr = elastic.setBlocks(hooke_double_ptr,hooke_adouble_ptr); CHKERRQ(ierr);
      ierr = elastic.addElement("ELASTIC","DISPLACEMENT"); CHKERRQ(ierr);

      //LAGMULT is added to ELASTIC element for post-processing only
      // ierr = m_field.modify_finite_element_add_field_data("ELASTIC","LAGMULT"); CHKERRQ(ierr);
      ierr = elastic.setOperators("DISPLACEMENT","MESH_NODE_POSITIONS",false,true); CHKERRQ(ierr);


      // Update material parameters
      for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,BLOCKSET,it)) {
        int id = it->getMeshsetId();
        if(block_data[id].yOung>0) {
          elastic.setOfBlocks[id].E = block_data[id].yOung;
          ierr = PetscPrintf(
            PETSC_COMM_WORLD,"Block %d set Young modulus %3.4g\n",id,elastic.setOfBlocks[id].E
          ); CHKERRQ(ierr);
        }
        if(block_data[id].pOisson>=-1) {
          elastic.setOfBlocks[id].PoissonRatio = block_data[id].pOisson;
          ierr = PetscPrintf(
            PETSC_COMM_WORLD,"Block %d set Poisson ratio %3.4g\n",id,elastic.setOfBlocks[id].PoissonRatio
          ); CHKERRQ(ierr);
        }
      }

      // Add Neumann forces
      ierr = MetaNeummanForces::addNeumannBCElements(m_field,"DISPLACEMENT"); CHKERRQ(ierr);



      //Range of flat prisms inserted between slave and master tris
      Range range_slave_master_prisms;

      ContactSearchKdTree contact_search_kd_tree(m_field);

      //Define multi-index conatiner to save these prisms and corresponding tris to be used for numerical integration of contact matrices
      ContactSearchKdTree::ContactCommonData_multiIndex contact_commondata_multi_index;

      //create kd_tree with master_surface only
      ierr = contact_search_kd_tree.buildTree(range_surf_master); CHKERRQ(ierr);

      //Fill this multi-index conainter contact_commondata_multi_index and range range_slave_master_prisms.
      //inputs  = [range_surf_master, range_surf_slave]
      //outputs = [contact_commondata_multi_index, range_slave_master_prisms]
      // this will be done with kd_tree search, efficient algorithm

      ierr = contact_search_kd_tree.contactSearchAlgorithm(range_surf_master, range_surf_slave, contact_commondata_multi_index, range_slave_master_prisms); CHKERRQ(ierr);
      cout<<"range_slave_master_prisms = "<<range_slave_master_prisms.size()<<endl;

      // // To cheack the twisted prisms (may be due to clockwise and anticlock wise connectiviy on -ve and +ve surfaces)
      // double coords[18];
      // for(Range::iterator it = range_slave_master_prisms.begin(); it!=range_slave_master_prisms.end();  it++) {
      //   const EntityHandle* conn_face;  int num_nodes;
      //   rval = moab.get_connectivity(*it,conn_face,num_nodes,true); CHKERRQ_MOAB(rval);
      //   rval = moab.get_coords(conn_face,num_nodes,coords); CHKERRQ_MOAB(rval);
      //   int count=0;
      //   for(int ii=0; ii<6; ii++){
      //     cerr<<coords[ii+count]<< "      "<<coords[ii+count+1]<< "      "<<coords[ii+count+2]<<endl;
      //     count=count+2;
      //   }
      //   string aaa;
      //   cin >> aaa;
      // }

      // // saving the polygon between master and slave tris
      // EntityHandle meshset_poly1; int count_poly=0;
      // rval = moab.create_meshset(MESHSET_SET,meshset_poly1); CHKERRQ_MOAB(rval);
      // for(Range::iterator it = range_slave_master_prisms.begin(); it!=range_slave_master_prisms.end();  it++) {
      //   ierr = m_field.get_moab().clear_meshset(&meshset_poly1,1); CHKERRQ(ierr);
      //   rval = moab.add_entities(meshset_poly1,&*it,1); CHKERRQ_MOAB(rval);
      //
      //   ostringstream sss1;
      //   sss1 << "poly_" << count_poly << ".vtk";
      //   rval = moab.write_file(sss1.str().c_str(),"VTK","",&meshset_poly1,1); CHKERRQ_MOAB(rval);
      //   count_poly++;
      // }

      // // saving the integration (common) tris belong to prisms one by one
      // Range range_poly_tris;  EntityHandle meshset_poly_tris;
      // rval = moab.create_meshset(MESHSET_SET,meshset_poly_tris); CHKERRQ_MOAB(rval);
      //
      // typedef ContactSearchKdTree::ContactCommonData_multiIndex::index<ContactSearchKdTree::Prism_tag>::type::iterator ItMultIndexPrism;
      // ItMultIndexPrism it_mult_index_prism    = contact_commondata_multi_index.get<ContactSearchKdTree::Prism_tag>().begin();
      // ItMultIndexPrism it_mult_index_prism_hi = contact_commondata_multi_index.get<ContactSearchKdTree::Prism_tag>().end();
      //
      // int count_poly=0;
      // for(; it_mult_index_prism != it_mult_index_prism_hi; it_mult_index_prism++) {
      //   cout<<"count_poly = "<<count_poly<<endl;
      //   range_poly_tris.clear();
      //   range_poly_tris=it_mult_index_prism->get()->commonIntegratedTriangle;
      //
      //   ierr = m_field.get_moab().clear_meshset(&meshset_poly_tris,1); CHKERRQ(ierr);
      //   rval = moab.add_entities(meshset_poly_tris,range_poly_tris); CHKERRQ_MOAB(rval);
      //
      //   ostringstream sss1;
      //   sss1 << "poly_tris" << count_poly << ".vtk";
      //   rval = moab.write_file(sss1.str().c_str(),"VTK","",&meshset_poly_tris,1); CHKERRQ_MOAB(rval);
      //   count_poly++;
      // }
      // // string aaa;
      // // cin >> aaa;


      //Add these prisim (between master and slave tris) to the mofem database
      EntityHandle meshset_slave_master_prisms;
      rval = moab.create_meshset(MESHSET_SET,meshset_slave_master_prisms); CHKERRQ_MOAB(rval);
      rval = moab.add_entities(meshset_slave_master_prisms,range_slave_master_prisms); CHKERRQ_MOAB(rval);
      ierr = m_field.seed_ref_level_3D(meshset_slave_master_prisms,bit_level0); CHKERRQ(ierr);
      ierr = m_field.get_entities_by_ref_level(bit_level0,BitRefLevel().set(),meshset_level0); CHKERRQ(ierr);

      rval = moab.write_mesh("slave_master_prisms.vtk", &meshset_slave_master_prisms, 1);
      //add contact element to be used to fill C and C^T matrices
      ContactProblemKdTreeNormalU  contact_problem(m_field, contact_commondata_multi_index);
      contact_problem.addContactElement("CONTACT_ELEM","DISPLACEMENT","LAGMULT",range_slave_master_prisms);

      //build field
      ierr = m_field.build_fields(); CHKERRQ(ierr);

      //get HO gemetry for 10 node tets
      // Projection10NodeCoordsOnField ent_method_material(m_field,"MESH_NODE_POSITIONS");
      // ierr = m_field.loop_dofs("MESH_NODE_POSITIONS",ent_method_material); CHKERRQ(ierr);

      //build finite elemnts
      ierr = m_field.build_finite_elements(); CHKERRQ(ierr);
      // string aaa;
      // cin >> aaa;

      //build adjacencies
      ierr = m_field.build_adjacencies(bit_level0); CHKERRQ(ierr);

      //define problems
      ierr = m_field.add_problem("CONTACT_PROB"); CHKERRQ(ierr);

      //set refinement level for problem
      ierr = m_field.modify_problem_ref_level_add_bit("CONTACT_PROB",bit_level0); CHKERRQ(ierr);

      DMType dm_name = "CONTACT_PROB";
      ierr = DMRegister_MoFEM(dm_name); CHKERRQ(ierr);

      //craete dm instance
      DM dm;
      ierr = DMCreate(PETSC_COMM_WORLD,&dm);CHKERRQ(ierr);
      ierr = DMSetType(dm,dm_name);CHKERRQ(ierr);
      //set dm datastruture whict created mofem datastructures
      ierr = DMMoFEMCreateMoFEM(dm,&m_field,dm_name,bit_level0); CHKERRQ(ierr);
      ierr = DMSetFromOptions(dm); CHKERRQ(ierr);
      ierr = DMMoFEMSetIsPartitioned(dm,is_partitioned); CHKERRQ(ierr);
      //add elements to dm
      ierr = DMMoFEMAddElement(dm,"ELASTIC"); CHKERRQ(ierr);
      ierr = DMMoFEMAddElement(dm,"FORCE_FE"); CHKERRQ(ierr);
      ierr = DMMoFEMAddElement(dm,"CONTACT_ELEM"); CHKERRQ(ierr);
      ierr = DMSetUp(dm); CHKERRQ(ierr);

      //create vectors and matrices
      Vec F,D,D0;
      ierr = DMCreateGlobalVector(dm,&F); CHKERRQ(ierr);
      ierr = VecDuplicate(F,&D); CHKERRQ(ierr);
      ierr = VecDuplicate(F,&D0); CHKERRQ(ierr);
      Mat Aij;
      ierr = DMCreateMatrix(dm,&Aij); CHKERRQ(ierr);
      ierr = MatSetOption(Aij,MAT_SPD,PETSC_TRUE); CHKERRQ(ierr);

      ierr = VecZeroEntries(F); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecZeroEntries(D); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = DMoFEMMeshToLocalVector(dm,D,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      ierr = MatZeroEntries(Aij); CHKERRQ(ierr);



      bool flag_cubit_disp = false;
      for(_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(m_field,NODESET|DISPLACEMENTSET,it)) {
        flag_cubit_disp = true;
      }

      //assemble Aij and F
      boost::shared_ptr<FEMethod> dirihlet_bc_ptr;
      // if normally defined boundary conditions are not found, try to use DISPLACEMENT blockset
      if(!flag_cubit_disp){
        dirihlet_bc_ptr = boost::shared_ptr<FEMethod>(new DirichletBCFromBlockSetFEMethodPreAndPostProcWithFlags(m_field,"DISPLACEMENT","DISPLACEMENT",Aij,D0,F));
      } else {
        dirihlet_bc_ptr = boost::shared_ptr<FEMethod>(new DisplacementBCFEMethodPreAndPostProc(m_field,"DISPLACEMENT",Aij,D0,F));
      }


      dirihlet_bc_ptr->snes_ctx = FEMethod::CTX_SNESNONE;
      dirihlet_bc_ptr->ts_ctx = FEMethod::CTX_TSNONE;

      ierr = VecZeroEntries(D0); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(D0,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(D0,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = DMoFEMMeshToLocalVector(dm,D0,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      ierr = DMoFEMPreProcessFiniteElements(dm,dirihlet_bc_ptr.get()); CHKERRQ(ierr);
      ierr = DMoFEMMeshToLocalVector(dm,D0,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      //ierr = VecView(D0,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);

      //internal force vector (to take into account Dirchelt boundary conditions)
      elastic.getLoopFeRhs().snes_f = F;
      ierr = DMoFEMLoopFiniteElements(dm,"ELASTIC",&elastic.getLoopFeRhs()); CHKERRQ(ierr);
      //ierr = VecView(F,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
      //elastic element matrix
      elastic.getLoopFeLhs().snes_B = Aij;
      ierr = DMoFEMLoopFiniteElements(dm,"ELASTIC",&elastic.getLoopFeLhs()); CHKERRQ(ierr);

      //forces and pressures on surface
      boost::ptr_map<std::string,NeummanForcesSurface> neumann_forces;
      ierr = MetaNeummanForces::setMomentumFluxOperators(m_field,neumann_forces,F,"DISPLACEMENT"); CHKERRQ(ierr);
      {
        boost::ptr_map<std::string,NeummanForcesSurface>::iterator mit = neumann_forces.begin();
        for(;mit!=neumann_forces.end();mit++) {
          cout << "Neumman BC " << endl;
          ierr = DMoFEMLoopFiniteElements(dm,mit->first.c_str(),&mit->second->getLoopFe()); CHKERRQ(ierr);
        }
      }

      double area_slave, area_master;
      area_master=0.0; area_slave=0.0;
      contact_problem.setContactOperators("DISPLACEMENT","LAGMULT",Aij,F,contact_commondata_multi_index,area_master,area_slave);
      ierr = DMoFEMLoopFiniteElements(dm,"CONTACT_ELEM",&contact_problem.getLoopFeContactLhs()); CHKERRQ(ierr);
      // cout << "area_master = " << area_master << endl;
      // cout << "area_slave = " << area_slave << endl;

      ierr = DMoFEMPostProcessFiniteElements(dm,dirihlet_bc_ptr.get()); CHKERRQ(ierr);

      ierr = VecGhostUpdateBegin(F,ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(F,ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      ierr = VecAssemblyBegin(F); CHKERRQ(ierr);
      ierr = VecAssemblyEnd(F); CHKERRQ(ierr);

      ierr = VecScale(F,-1); CHKERRQ(ierr);

      ierr = MatAssemblyBegin(Aij,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      ierr = MatAssemblyEnd(Aij,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

      // MatView(Aij,PETSC_VIEWER_DRAW_WORLD);
      // VecView(F,PETSC_VIEWER_STDOUT_WORLD);
      //
      // string wait;
      // cin >> wait;

      // Solver
      KSP solver;
      ierr = KSPCreate(PETSC_COMM_WORLD,&solver); CHKERRQ(ierr);
      ierr = KSPSetOperators(solver,Aij,Aij); CHKERRQ(ierr);
      ierr = KSPSetFromOptions(solver); CHKERRQ(ierr);
      ierr = KSPSetUp(solver); CHKERRQ(ierr);

      PostProcVolumeOnRefinedMesh post_proc(m_field);
      ierr = post_proc.generateReferenceElementMesh(); CHKERRQ(ierr);
      ierr = post_proc.addFieldValuesPostProc("DISPLACEMENT"); CHKERRQ(ierr);

      // ierr = post_proc.addFieldValuesPostProc("MESH_NODE_POSITIONS"); CHKERRQ(ierr);
      ierr = post_proc.addFieldValuesGradientPostProc("DISPLACEMENT"); CHKERRQ(ierr);
      //add postpocessing for sresses
      post_proc.getOpPtrVector().push_back(
        new PostPorcHookStress(
    	    m_field,
    	    post_proc.postProcMesh,
    	    post_proc.mapGaussPts,
    	    "DISPLACEMENT",
    	    post_proc.commonData,
          &elastic.setOfBlocks
        )
      );

    ierr = KSPSolve(solver,F,D); CHKERRQ(ierr);
    ierr = VecAXPY(D,1.,D0); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

    //Save data on mesh
    // VecView(D,PETSC_VIEWER_STDOUT_WORLD);

    ierr = DMoFEMMeshToLocalVector(dm,D,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
    ierr = DMoFEMLoopFiniteElements(dm,"ELASTIC",&post_proc); CHKERRQ(ierr);
    ierr = post_proc.writeFile("out.h5m"); CHKERRQ(ierr);

    PostProcFatPrismOnRefinedMesh prism_post_proc(m_field);
    ierr = prism_post_proc.addFieldValuesPostProc("DISPLACEMENT"); CHKERRQ(ierr);
    ierr = DMoFEMLoopFiniteElements(dm,"ELASTIC",&prism_post_proc); CHKERRQ(ierr);
    ierr = post_proc.writeFile("out_prisms.h5m"); CHKERRQ(ierr);



    //Active set vector (+ve active,   -ve inactive)
    Vec Fg; //size of this vector is equal to the global size
    ierr = DMCreateGlobalVector(dm,&Fg); CHKERRQ(ierr);
    ierr = VecZeroEntries(Fg); CHKERRQ(ierr);

    // ContactProblemActiveSet  contact_active_set(m_field, contact_commondata_multi_index);
    contact_problem.setContactOperatorsActiveSet("DISPLACEMENT","LAGMULT",Fg);
    ierr = DMoFEMLoopFiniteElements(dm,"CONTACT_ELEM",&contact_problem.getLoopFeActiveSetRhs()); CHKERRQ(ierr);
    VecView(Fg,PETSC_VIEWER_STDOUT_WORLD);


    //post-processing lagrange multipliers and gap-functions for active set search
    const Problem *problem_ptr;
    m_field.get_problem ("CONTACT_PROB", &problem_ptr);
    double *array_lagmul, *array_fg;
    VecGetArray(D,&array_lagmul);
    VecGetArray(Fg,&array_fg);
    for(_IT_NUMEREDDOF_ROW_BY_NAME_FOR_LOOP_(problem_ptr, "LAGMULT", dof_ptr)){
      int local_index = dof_ptr->get()->getPetscLocalDofIdx();
      if(local_index<0) continue; // is not on this partition
      double val_lagmul = array_lagmul[local_index]; // get value from vector
      double val_fg = array_fg[local_index]; // get value from vector
      // cout << "val = "<<val <<endl;
      EntityHandle ent = dof_ptr->get()->getEnt();

      Tag tag_lagmul, tag_fg;
      double def_val = 0;
      rval = moab.tag_get_handle("LAGMUL",1,MB_TYPE_DOUBLE,tag_lagmul,MB_TAG_CREAT|MB_TAG_SPARSE,&def_val); CHKERRQ_MOAB(rval);
      rval = moab.tag_get_handle("GAPFUN",1,MB_TYPE_DOUBLE,tag_fg,    MB_TAG_CREAT|MB_TAG_SPARSE,&def_val); CHKERRQ_MOAB(rval);
      rval = moab.tag_set_data(tag_lagmul,&ent,1,&val_lagmul); CHKERRQ_MOAB(rval);
      rval = moab.tag_set_data(tag_fg,    &ent,1,&val_fg); CHKERRQ_MOAB(rval);
    }
    VecRestoreArray(D,&array_lagmul);
    VecRestoreArray(Fg,&array_fg);

    EntityHandle meshset_slave_tris_post_process;
    rval = moab.create_meshset(MESHSET_SET,meshset_slave_tris_post_process); CHKERRQ_MOAB(rval);
    rval = moab.add_entities(meshset_slave_tris_post_process,range_surf_slave); CHKERRQ_MOAB(rval);
    rval = moab.write_mesh("slave_tris_post_process.vtk", &meshset_slave_tris_post_process, 1);



  PetscFinalize();
  return 0;
}
