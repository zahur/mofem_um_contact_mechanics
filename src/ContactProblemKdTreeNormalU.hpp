/* This file is part of MoFEM.
* MoFEM is free software: you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the
* Free Software Foundation, either version 3 of the License, or (at your
* option) any later version.
*
* MoFEM is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */


#ifdef __cplusplus
extern "C" {
  #endif
  #include <cblas.h>
  // #include <lapack_wrap.h>
  // #include <gm_rule.h>
  #include <quad.h>
  #ifdef __cplusplus
}
#endif


struct ContactProblemKdTreeNormalU: public  ContactProblemKdTree{


  ContactProblemKdTreeNormalU(MoFEM::Interface &m_field, ContactSearchKdTree::ContactCommonData_multiIndex &contact_commondata_multi_index):
  ContactProblemKdTree(m_field, contact_commondata_multi_index),
  feActiveSetRhs(m_field, contact_commondata_multi_index){
  }


  ConctactElement feActiveSetRhs;
  ConctactElement& getLoopFeActiveSetRhs() { return feActiveSetRhs; }


  struct CommonFunctionsContact {
      PetscErrorCode calculateDn_dx(
        MatrixDouble &dn_dx, CommonDataContact &commonDataContact,EntityType col_type,
        DataForcesAndSurcesCore::EntData &col_data, int nb_base_fun_col, int gg, int nb_gauss_pts,
        int face_tri
      ) {
        PetscFunctionBegin;

        

        MatrixDouble dn_dx1, dn_dx2;
        dn_dx1.resize(3, 3*nb_base_fun_col);
        dn_dx2.resize(3, 3*nb_base_fun_col);
        dn_dx1.clear(); dn_dx2.clear();

        MatrixDouble spin_s1, spin_s2;
        spin_s1.resize(3,3);  spin_s2.resize(3,3);
        spin_s1.clear(); spin_s2.clear();

        // cerr << "t1 "<<  commonDataContact.tangentF4[gg][0] <<  endl;
        // cerr << "t2 "<<  commonDataContact.tangentF4[gg][1] <<  endl;
        if(face_tri==3){
          ierr = Spin(&*spin_s1.data().begin(),&*commonDataContact.tangentF3[gg][0].data().begin()); CHKERRQ(ierr);
          ierr = Spin(&*spin_s2.data().begin(),&*commonDataContact.tangentF3[gg][1].data().begin()); CHKERRQ(ierr);
        }else if(face_tri==4){
          ierr = Spin(&*spin_s1.data().begin(),&*commonDataContact.tangentF4[gg][0].data().begin()); CHKERRQ(ierr);
          ierr = Spin(&*spin_s2.data().begin(),&*commonDataContact.tangentF4[gg][1].data().begin()); CHKERRQ(ierr);
        }
        // cerr << "n  "<<  prod(spin_s1, commonDataContact.tangentF4[gg][1]) <<  endl;
        // cerr << "spin_s1 "<<  spin_s1 <<  endl;
        // cerr << "spin_s2 "<<  spin_s2 <<  endl;
        // cerr << "col_data.getDiffN() "<<  col_data.getDiffN() <<  endl;

        //this here is done for column
        //nodes getDiffN()=[dN1/dxi, dN1/deta, dN2/dxi, dN2/deta, dN3/dxi, dN3/deta]
        //Edges etc getDiffN()=[dN1/dxi, dN1/deta]
        // cerr << "nb_base_fun_col "<<  nb_base_fun_col      <<  endl;
        if(col_type==MBVERTEX){ //as dn/dx only for slave cols
          for(int rr=0; rr<3; rr++){
            for(int cc=0; cc<3; cc++){
              int count_bf=0;
              for(int bf=0; bf<nb_base_fun_col; bf++){
                // cerr << "rr "<<  rr      <<  endl;
                // cerr << "cc "<<  cc      <<  endl;
                // cerr << "bf+1 "<<  bf+1 <<  endl;
                // cerr << "3*cc+bf "<<  3*cc+bf <<  endl;
                if(face_tri==3){
                  dn_dx1(rr, nb_base_fun_col*cc+bf)=spin_s1(rr,cc) * col_data.getDiffN()(gg,0+count_bf+1);
                  dn_dx2(rr, nb_base_fun_col*cc+bf)=spin_s2(rr,cc) * col_data.getDiffN()(gg,0+count_bf);
                }else if(face_tri==4){
                  dn_dx1(rr, nb_base_fun_col*cc+bf)=spin_s1(rr,cc) * col_data.getDiffN()(nb_gauss_pts/2+gg,6+count_bf+1);
                  dn_dx2(rr, nb_base_fun_col*cc+bf)=spin_s2(rr,cc) * col_data.getDiffN()(nb_gauss_pts/2+gg,6+count_bf);
                }
                count_bf+=2;
              }
            }
          }
        }else{
          for(int rr=0; rr<3; rr++){
            for(int cc=0; cc<3; cc++){
              int count_bf=0;
              for(int bf=0; bf<nb_base_fun_col; bf++){
                // cerr << "rr "<<  rr      <<  endl;
                // cerr << "cc "<<  cc      <<  endl;
                // cerr << "nb_base_fun_col*cc+bf "<<  nb_base_fun_col*cc+bf <<  endl;
                if(face_tri==3){
                  dn_dx1(rr, nb_base_fun_col*cc+bf)=spin_s1(rr,cc) * col_data.getDiffN()(gg,count_bf+1);
                  dn_dx2(rr, nb_base_fun_col*cc+bf)=spin_s2(rr,cc) * col_data.getDiffN()(gg,count_bf);
                }else if(face_tri==4){
                  dn_dx1(rr, nb_base_fun_col*cc+bf)=spin_s1(rr,cc) * col_data.getDiffN()(nb_gauss_pts/2+gg,count_bf+1);
                  dn_dx2(rr, nb_base_fun_col*cc+bf)=spin_s2(rr,cc) * col_data.getDiffN()(nb_gauss_pts/2+gg,count_bf);
                }
                count_bf+=2;
              }
            }
          }
        }
        // cerr << "dn_dx1 "<<  dn_dx1 <<  endl;
        // cerr << "dn_dx2 "<<  dn_dx2 <<  endl;
        // string aaa;
        // cin>> aaa;

        dn_dx=dn_dx1-dn_dx2;
        // cerr << "dn_dx "<<  dn_dx <<  endl;
        PetscFunctionReturn(0);
      }
  };
  CommonFunctionsContact commonFunctionsContact;



  /// \biref operator to calculate and assemble Cmat for contact
  struct OpContactConstraintMatrix:public FlatPrismElementForcesAndSurcesCore::UserDataOperator  {

    Mat Aij;
    OpContactConstraintMatrix(
      const string field_name,
      const string lagrang_field_name,
      Mat aij = PETSC_NULL
    ):
    FlatPrismElementForcesAndSurcesCore::UserDataOperator(
      lagrang_field_name,field_name, UserDataOperator::OPROWCOL
    ),
    Aij(aij)
    {
      sYmm = false;  //This will make sure to loop over all intities (e.g. for order=2 it will make doWork to loop 16 time)
    }
    MatrixDouble NN,transNN;
    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSurcesCore::EntData &row_data,
      DataForcesAndSurcesCore::EntData &col_data
    ) {
      PetscFunctionBegin;
      try {
        //As lagrange multipliers are only sitting on the slave/top side
        if(row_type == MBEDGE && row_side < 6) PetscFunctionReturn(0);
        if(row_type == MBTRI && row_side == 3) PetscFunctionReturn(0);
        
        

        const int nb_row = row_data.getIndices().size();
        if(!nb_row) PetscFunctionReturn(0);
        const int nb_col = col_data.getIndices().size();
        if(!nb_col) PetscFunctionReturn(0);
        const int nb_gauss_pts = row_data.getN().size1();

        int shift_row = 0;
        int nb_base_fun_row = row_data.getN().size2();
        if(row_type == MBVERTEX) {
          shift_row = 3;
          nb_base_fun_row = 3;  //total are 6 for MBVERTEX (top and bottom tris)
        }

        int shift_col = 0;
        int nb_base_fun_col = col_data.getN().size2();
        if(col_type == MBVERTEX) {
          shift_col = 3;
          nb_base_fun_col = 3; //total are 6 for MBVERTEX (top and bottom tris)
        }
        // cerr << "nb_base_fun_row  "<< nb_base_fun_row <<  endl;
        // cerr << "nb_base_fun_col  "<< nb_base_fun_col <<  endl;

        // cerr << "3*nb_base_fun_row  "<< 3*nb_base_fun_row <<  endl;
        // cerr << "2*(3*nb_base_fun_col)  "<< 2*(3*nb_base_fun_col) <<  endl;
        //NN is matrix first half of it belong to master/bottom tri and the sencond half is blong to slave tri
        const double *normal_f3_ptr = &getNormalF3()[0];
        const double area_m = cblas_dnrm2(3,normal_f3_ptr,1)*0.5; //master tri area
        const double *normal_f4_ptr = &getNormalF4()[0];
        const double area_s = cblas_dnrm2(3,normal_f4_ptr,1)*0.5; //slave tri area
        // cerr << "area_m  "<< area_m <<  endl;
        // cerr << "area_s  "<< area_s <<  endl;


        // //unit normals to both f3 and f4
        VectorDouble n4=getNormalF4();
        // cout<<"n3 = "<<n3<<endl;
        double n4_mag;
        n4_mag=sqrt(n4[0]*n4[0] + n4[1]*n4[1] + n4[2]*n4[2]);
        // cout<<"n3_mag = "<<n3_mag<<endl;
        VectorDouble n4_unit;  n4_unit.resize(3, false);
        for(int ii=0; ii<3; ii++){
          n4_unit[ii]=n4[ii]/n4_mag;
        }
        // cout<<"n4_unit = "<<n4_unit<<endl;

        //flag master used to identify master and slave edges and faces (used to assemble edges/faces as m is -ve and s is +ve)
        bool master;
        if(
          (col_type == MBEDGE && col_side <= 3)||
          (col_type == MBTRI && col_side == 3)
        ) master = true;
        else master = false;

        //this should be here outside the gauss points loops
        // we use NN[1x9 1x9] matrix for vertices as we can see all at once, i.e. NN[3*nb_base_fun_row, 2*(3*nb_base_fun_col)]
        // 1 here because only one LAGMULT per node and 3 DISPLACEMENT component per node
        // for the rest we will use NN[1, 3*nb_base_fun_col]
        if(col_type == MBVERTEX) {
          //as we can see all nodes (belong to both tris) at once for MBVERTEX so size of NN is (*nb_base_fun_row, 2*(3*nb_base_fun_col)
          NN.resize(nb_base_fun_row, 2*(3*nb_base_fun_col),false); //the last false in ublas resize will destroy (not preserved) the old values
          NN.clear();
          transNN.resize(2*(3*nb_base_fun_col),nb_base_fun_row,false);
          transNN.clear();
        }else{
          //size of NN is different than the MBVERTEX, as we cannot see entities of both(top/bottom) tris here
          NN.resize(nb_base_fun_row, 3*nb_base_fun_col,false);
          NN.clear();
          transNN.resize(3*nb_base_fun_col,nb_base_fun_row,false);
          transNN.clear();
        }
        // cerr << "col_side  "<< col_side <<  endl;
        // cerr << "col_side  "<< col_side <<  endl;
        // cerr << "nb_gauss_pts  "<< nb_gauss_pts <<  endl;

        //loop over half of the gauss points, as we will do all caluclation (belong to both bottom/top or master/slave) in this
        for(int gg = 0;gg!=nb_gauss_pts/2;gg++) {
          double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*area_s;
          double val_m = getGaussPts()(2,gg)*area_m;

          // cerr << "col_data.getN() "<<  col_data.getN() <<  endl;
          // cerr << "row_data.getN()  "<< row_data.getN() <<  endl;
          // cerr << "val_s "<< val_s <<  endl;
          // cerr << "val_m "<< val_m <<  endl;
          //ftensor pointer pointing to the shape functions for  master and slave sides
          //(first set of guass points belong to master and the second half belong to slave)
          FTensor::Tensor0<double*> t_base_master(&col_data.getN()(gg,0));
          FTensor::Tensor0<double*> t_base_slave(&col_data.getN()(nb_gauss_pts/2+gg,shift_col));
          // cerr << "col_data.getN()  "<< col_data.getN()(gg,0) <<  endl;
          // cerr << "col_data.getN() shift_col "<< col_data.getN()(nb_gauss_pts/2+gg,shift_col) <<  endl;

          //in this if (we will calculate and assemble both m and s to the NN as we can see all the nodes of the prism)
          if(col_type == MBVERTEX) {
            // cerr << "NN  "<< NN <<  endl;
            for(int bbc=0; bbc!=nb_base_fun_col; bbc++) {
                FTensor::Tensor0<double*> t_base_lambda(&row_data.getN()(nb_gauss_pts/2+gg,shift_row));
                // cerr << "row_data.getN()(  "<< row_data.getN()(nb_gauss_pts/2+gg,shift_row) <<  endl;
                for(int bbr=0; bbr!=nb_base_fun_row; bbr++) {
                  const double m = val_m*t_base_lambda*t_base_master;
                  const double s = val_s*t_base_lambda*t_base_slave;
                  for(int dd=0; dd<3; dd++){
                    // cerr << "3*bbc+dd  "<< 3*bbc+dd <<  endl;
                    // cerr << "t_base_lambda   = "<< t_base_lambda <<  endl;
                    // cerr << "t_base_master   = "<< t_base_master <<  endl;
                    NN(bbr,3*bbc+dd) -= m*n4_unit[dd];
                    NN(bbr,3*nb_base_fun_col+3*bbc+dd) += s*n4_unit[dd];
                  }
                  ++t_base_lambda; //update rows
                }
                ++t_base_master; //update cols master
                ++t_base_slave;  //update cols slave
              }
              // cerr << "NN  "<< NN <<  endl;
              // string aaa;
              // cin >> aaa;
            }
          else {  //in the elase part we will either assemble m or s depending upon the entinity (which can belong to either master or slave tri)
            for(int bbc=0; bbc!=nb_base_fun_col; bbc++) {
                FTensor::Tensor0<double*> t_base_lambda(&row_data.getN()(nb_gauss_pts/2+gg,shift_row));
                // cerr << "t_base_master  "<< t_base_master <<  endl;
                // cerr << "row_data.getN()(  "<< row_data.getN()(nb_gauss_pts/2+gg,shift_row) <<  endl;
                for(int bbr=0; bbr!=nb_base_fun_row; bbr++) {
                  // cerr << "t_base_lambda  "<< t_base_lambda <<  endl;
                  const double m = val_m*t_base_lambda*t_base_master;
                  const double s = val_s*t_base_lambda*t_base_slave;

                  for(int dd=0; dd<3; dd++){
                    if(master){
                      NN(bbr,3*bbc+dd) -= m*n4_unit[dd];
                    }else{
                      NN(bbr,3*bbc+dd) += s*n4_unit[dd];
                    }
                  }
                  ++t_base_lambda;
                }
                ++t_base_master;
                ++t_base_slave;
              }
              // cerr << "val_m  "<< val_m <<  endl;
              // cerr << "row_data.getN()  "<< row_data.getN() <<  endl;
              // cerr << "col_data.getN()  "<< col_data.getN() <<  endl;
            }
          }

        //
        // }
        if(Aij == PETSC_NULL) {
          Aij = getFEMethod()->snes_B;
        }
        // cerr << "NN  "<< NN <<  endl;
        // cerr << "row_data.getIndices()     "<< row_data.getIndices() <<  endl;
        // cerr << "col_data.getIndices()     "<< col_data.getIndices() <<  endl;
        // string aaa;
        // cin >> aaa;
        //Assemble NN to final Aij vector based on its global indices
        ierr = MatSetValues(
          Aij,
          nb_base_fun_row,&row_data.getIndices()[shift_row],
          nb_col,&col_data.getIndices()[0],
          &*NN.data().begin(),ADD_VALUES
        ); CHKERRQ(ierr);


        //Calculae and assemble trans(NN) and assemble it Aij based on its global indices
        noalias(transNN) = trans(NN);
        // cerr << "transNN  "<< transNN <<  endl;
        ierr = MatSetValues(
          Aij,
          nb_col,&col_data.getIndices()[0],
          nb_base_fun_row,&row_data.getIndices()[shift_row],
          &*transNN.data().begin(),ADD_VALUES
        ); CHKERRQ(ierr);

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }
      PetscFunctionReturn(0);
    }
  };





  /// \biref tangents t1 and t2 to face f4 at all gauss points
  struct OpGetTangent:public FlatPrismElementForcesAndSurcesCore::UserDataOperator  {

    CommonDataContact &commonDataContact;
    OpGetTangent(
      const string field_name,
      CommonDataContact &common_data_contact
    ):
    FlatPrismElementForcesAndSurcesCore::UserDataOperator(
      field_name, UserDataOperator::OPCOL
    ),
    commonDataContact(common_data_contact){
    }

    int ngp;
    PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;
      try {

        if(data.getFieldData().size()==0)  PetscFunctionReturn(0);
        // if(type == MBEDGE && side < 6) PetscFunctionReturn(0);
        // if(type == MBTRI &&  side == 3) PetscFunctionReturn(0);
        if(type==MBVERTEX){
            // cerr << "OpGetTangent" <<endl;
            ngp=data.getN().size1(); // this is number of base functions for vertices, 6 bases functions for prism
            commonDataContact.tangentF3.resize(ngp/2);
            commonDataContact.tangentF4.resize(ngp/2);

            // tangent vectors to face F3
            for(unsigned int gg=0; gg<ngp/2; gg++) {
              commonDataContact.tangentF3[gg].resize(2);
              commonDataContact.tangentF3[gg][0].resize(3);
              commonDataContact.tangentF3[gg][1].resize(3);
              for(int dd = 0;dd<3;dd++) {
                commonDataContact.tangentF3[gg][0][dd] = cblas_ddot(3,&data.getDiffN()(gg,0),2,&data.getFieldData()[dd],3); //tangent-1
                commonDataContact.tangentF3[gg][1][dd] = cblas_ddot(3,&data.getDiffN()(gg,1),2,&data.getFieldData()[dd],3); //tangent-2
              }
            }

            //tangent vectors to face F4
            for(unsigned int gg=0; gg<ngp/2; gg++) {
              commonDataContact.tangentF4[gg].resize(2);
              commonDataContact.tangentF4[gg][0].resize(3);
              commonDataContact.tangentF4[gg][1].resize(3);
              for(int dd = 0;dd<3;dd++) {
                commonDataContact.tangentF4[gg][0][dd] = cblas_ddot(3,&data.getDiffN()(ngp/2+gg,6+0),2,&data.getFieldData()[9+dd],3); //tangent-1
                commonDataContact.tangentF4[gg][1][dd] = cblas_ddot(3,&data.getDiffN()(ngp/2+gg,6+1),2,&data.getFieldData()[9+dd],3); //tangent-2
              }
            }
          }
          //tangent vectors to face F3
          else if ((type==MBEDGE && side<=3) || (type==MBTRI &&  side==3)){//master (lower triangle)
            unsigned int nb_dofs = data.getFieldData().size();
            for(unsigned int gg=0; gg<ngp/2; gg++) {
              for(unsigned int dd=0; dd<3; dd++) {
                commonDataContact.tangentF3[gg][0][dd] += cblas_ddot(nb_dofs/3,&data.getDiffN()(gg,0),2,&data.getFieldData()[dd],3); //tangent-1
                commonDataContact.tangentF3[gg][1][dd] += cblas_ddot(nb_dofs/3,&data.getDiffN()(gg,1),2,&data.getFieldData()[dd],3); //tangent-2
              }
            }
          }
          //tangent vectors to face F4
          else if((type==MBEDGE && side>6) || (type==MBTRI &&  side==4)){ //slave (upper triangle)
            unsigned int nb_dofs = data.getFieldData().size();
            for(unsigned int gg=0; gg<ngp/2; gg++) {
              for(unsigned int dd=0; dd<3; dd++) {
                commonDataContact.tangentF4[gg][0][dd] += cblas_ddot(nb_dofs/3,&data.getDiffN()(ngp/2+gg,0),2,&data.getFieldData()[dd],3); //tangent-1
                commonDataContact.tangentF4[gg][1][dd] += cblas_ddot(nb_dofs/3,&data.getDiffN()(ngp/2+gg,1),2,&data.getFieldData()[dd],3); //tangent-2
              }
            }
          }
      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }
      PetscFunctionReturn(0);
    }
  };



  /// \biref tangents t1 and t2 to face f4 at all gauss points
  struct OpGetNormal:public FlatPrismElementForcesAndSurcesCore::UserDataOperator  {

    CommonDataContact &commonDataContact;
    OpGetNormal(
      const string field_name,
      CommonDataContact &common_data_contact
    ):
    FlatPrismElementForcesAndSurcesCore::UserDataOperator(
      field_name, UserDataOperator::OPCOL
    ),
    commonDataContact(common_data_contact){
    }

    PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;
      try {
        
        if(type==MBVERTEX) {
          // cerr << "OpGetNormal" <<endl;
          int ngp_1_2 = commonDataContact.tangentF4.size(); //this is ngp/2
          commonDataContact.normalF3.resize(ngp_1_2);
          commonDataContact.normalF4.resize(ngp_1_2);
          MatrixDouble spin; spin.resize(3,3,false);
          for(int gg=0; gg<ngp_1_2; gg++){
            // cerr << "commonDataContact.tangentF3(gg)(0)  = "<<commonDataContact.tangentF3[gg][0]<<endl;
            // cerr << "commonDataContact.tangentF3(gg)(1)  = "<<commonDataContact.tangentF3[gg][1]<<endl;
            spin.clear();
            ierr = Spin(&*spin.data().begin(),&*commonDataContact.tangentF3[gg][0].data().begin()); CHKERRQ(ierr);
            // n= t1 x t2 =  spin(t1)t2
            commonDataContact.normalF3[gg]=prod(spin, commonDataContact.tangentF3[gg][1]);
            // cerr << "commonDataContact.normalF3[gg]  = " <<commonDataContact.normalF3[gg]<<endl;

            // cerr << "commonDataContact.tangentF4(gg)(0)  = "<<commonDataContact.tangentF4[gg][0]<<endl;
            // cerr << "commonDataContact.tangentF4(gg)(1)  = "<<commonDataContact.tangentF4[gg][1]<<endl;
            spin.clear();
            ierr = Spin(&*spin.data().begin(),&*commonDataContact.tangentF4[gg][0].data().begin()); CHKERRQ(ierr);
            // n= t1 x t2 =  spin(t1)t2
            commonDataContact.normalF4[gg]=prod(spin, commonDataContact.tangentF4[gg][1]);
            // cerr << "commonDataContact.normalF4[gg]  = " <<commonDataContact.normalF4[gg]<<endl;
            // string aaa;
            // cin >> aaa;
          }
        }
      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }
      PetscFunctionReturn(0);
    }
  };


  /// \biref operator to calculate and assemble Cmat for contact
  struct OpContactConstraintMatrixCandCT:public FlatPrismElementForcesAndSurcesCore::UserDataOperator  {

    Mat Aij;
    CommonDataContact &commonDataContact;
    OpContactConstraintMatrixCandCT(
      const string field_name,
      const string lagrang_field_name,
      CommonDataContact &common_data_contact,
      Mat aij = PETSC_NULL
    ):
    FlatPrismElementForcesAndSurcesCore::UserDataOperator(
      lagrang_field_name,field_name, UserDataOperator::OPROWCOL
    ),
    Aij(aij),
    commonDataContact(common_data_contact)
    {
      sYmm = false;  //This will make sure to loop over all entities (e.g. for order=2 it will make doWork to loop 16 time)
    }
    MatrixDouble NN,transNN;
    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSurcesCore::EntData &row_data,
      DataForcesAndSurcesCore::EntData &col_data
    ) {
      PetscFunctionBegin;
      try {
        //As lagrange multipliers are only sitting on the slave/top side
        if(row_type == MBEDGE && row_side < 6) PetscFunctionReturn(0);
        if(row_type == MBTRI && row_side == 3) PetscFunctionReturn(0);
        
        

        const int nb_row = row_data.getIndices().size();
        if(!nb_row) PetscFunctionReturn(0);
        const int nb_col = col_data.getIndices().size();
        if(!nb_col) PetscFunctionReturn(0);
        const int nb_gauss_pts = row_data.getN().size1();

        int shift_row = 0;
        int nb_base_fun_row = row_data.getN().size2();
        if(row_type == MBVERTEX) {
          shift_row = 3;
          nb_base_fun_row = 3;  //total are 6 for MBVERTEX (top and bottom tris)
        }

        int shift_col = 0;
        int nb_base_fun_col = col_data.getN().size2();
        if(col_type == MBVERTEX) {
          shift_col = 3;
          nb_base_fun_col = 3; //total are 6 for MBVERTEX (top and bottom tris)
        }
        // cerr << "nb_base_fun_row  "<< nb_base_fun_row <<  endl;
        // cerr << "nb_base_fun_col  "<< nb_base_fun_col <<  endl;

        // cerr << "3*nb_base_fun_row  "<< 3*nb_base_fun_row <<  endl;
        // cerr << "2*(3*nb_base_fun_col)  "<< 2*(3*nb_base_fun_col) <<  endl;
        //NN is matrix first half of it belong to master/bottom tri and the sencond half is blong to slave tri
        // const double *normal_f3_ptr = &getNormalF3()[0];
        // const double area_m = cblas_dnrm2(3,normal_f3_ptr,1)*0.5; //master tri area
        // const double *normal_f4_ptr = &getNormalF4()[0];
        // const double area_s = cblas_dnrm2(3,normal_f4_ptr,1)*0.5; //slave tri area
        // cerr << "area_m  "<< area_m <<  endl;
        // cerr << "area_s  "<< area_s <<  endl;

        //flag master used to identify master and slave edges and faces (used to assemble edges/faces as m is -ve and s is +ve)
        bool master;
        if(
          (col_type == MBEDGE && col_side <= 3)||
          (col_type == MBTRI && col_side == 3)
        ) master = true;
        else master = false;

        //this should be here outside the gauss points loops
        // we use NN[3x18 3x18] matrix for vertices as we can see all at once, i.e. NN[3*nb_base_fun_row, 2*(3*nb_base_fun_col)]
        // 1 here because only one LAGMULT per node and 3 DISPLACEMENT component per node
        // for the rest we will use NN[1, 3*nb_base_fun_col]
        if(col_type == MBVERTEX) {
          //as we can see all nodes (belong to both tris) at once for MBVERTEX so size of NN is (*nb_base_fun_row, 2*(3*nb_base_fun_col)
          NN.resize(nb_base_fun_row, 2*(3*nb_base_fun_col),false); //the last false in ublas resize will destroy (not preserved) the old values
          NN.clear();
          transNN.resize(2*(3*nb_base_fun_col),nb_base_fun_row,false);
          transNN.clear();
        }else{
          //size of NN is different than the MBVERTEX, as we cannot see entities of both(top/bottom) tris here
          NN.resize(nb_base_fun_row, 3*nb_base_fun_col,false);
          NN.clear();
          transNN.resize(3*nb_base_fun_col,nb_base_fun_row,false);
          transNN.clear();
        }
        // cerr << "col_side  "<< col_side <<  endl;
        // cerr << "col_side  "<< col_side <<  endl;
        // cerr << "nb_gauss_pts  "<< nb_gauss_pts <<  endl;

        VectorDouble normal_f4;
        //loop over half of the gauss points, as we will do all caluclation (belong to both bottom/top or master/slave) in this
        for(int gg = 0;gg!=nb_gauss_pts/2;gg++) {
          const double area_m = norm_2(commonDataContact.normalF3[gg])*0.5; //master tri area
          const double area_s = norm_2(commonDataContact.normalF4[gg])*0.5; //master tri area
          // double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*area_s;
          // double val_m = getGaussPts()(2,gg)*area_m;
          //integration over parent element
          double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*0.5;
          double val_m = getGaussPts()(2,gg)*0.5;

          normal_f4=commonDataContact.normalF4[gg];
          // cerr << "normal_f4 ======== "<< normal_f4 <<  endl;
          // string aaa;
          // cin >> aaa;

          // cerr << "col_data.getN() "<<  col_data.getN() <<  endl;
          // cerr << "row_data.getN()  "<< row_data.getN() <<  endl;
          // cerr << "val_s "<< val_s <<  endl;
          // cerr << "val_m "<< val_m <<  endl;
          //ftensor pointer pointing to the shape functions for  master and slave sides
          //(first set of guass points belong to master and the second half belong to slave)
          FTensor::Tensor0<double*> t_base_master(&col_data.getN()(gg,0));
          FTensor::Tensor0<double*> t_base_slave(&col_data.getN()(nb_gauss_pts/2+gg,shift_col));
          // cerr << "col_data.getN()  "<< col_data.getN()(gg,0) <<  endl;
          // cerr << "col_data.getN() shift_col "<< col_data.getN()(nb_gauss_pts/2+gg,shift_col) <<  endl;

          //in this if (we will calculate and assemble both m and s to the NN as we can see all the nodes of the prism)
          if(col_type == MBVERTEX) {
            // cerr << "NN  "<< NN <<  endl;
            for(int bbc=0; bbc!=nb_base_fun_col; bbc++) {
                FTensor::Tensor0<double*> t_base_lambda(&row_data.getN()(nb_gauss_pts/2+gg,shift_row));
                // cerr << "row_data.getN()(  "<< row_data.getN()(nb_gauss_pts/2+gg,shift_row) <<  endl;
                for(int bbr=0; bbr!=nb_base_fun_row; bbr++) {
                  const double m = val_m*t_base_lambda*t_base_master*(area_m/area_s);
                  const double s = val_s*t_base_lambda*t_base_slave;
                  for(int dd=0; dd<3; dd++){
                    // cerr << "3*bbc+dd  "<< 3*bbc+dd <<  endl;
                    // cerr << "t_base_lambda   = "<< t_base_lambda <<  endl;
                    // cerr << "t_base_master   = "<< t_base_master <<  endl;
                    NN(bbr,3*bbc+dd) -= m*normal_f4[dd];
                    NN(bbr,3*nb_base_fun_col+3*bbc+dd) += s*normal_f4[dd];
                  }
                  ++t_base_lambda; //update rows
                }
                ++t_base_master; //update cols master
                ++t_base_slave;  //update cols slave
              }
              // cerr << "NN  "<< NN <<  endl;
              // string aaa;
              // cin >> aaa;
            }
          else {  //in the elase part we will either assemble m or s depending upon the entinity (which can belong to either master or slave tri)
            for(int bbc=0; bbc!=nb_base_fun_col; bbc++) {
                FTensor::Tensor0<double*> t_base_lambda(&row_data.getN()(nb_gauss_pts/2+gg,shift_row));
                // cerr << "t_base_master  "<< t_base_master <<  endl;
                // cerr << "row_data.getN()(  "<< row_data.getN()(nb_gauss_pts/2+gg,shift_row) <<  endl;
                for(int bbr=0; bbr!=nb_base_fun_row; bbr++) {
                  // cerr << "t_base_lambda  "<< t_base_lambda <<  endl;
                  const double m = val_m*t_base_lambda*t_base_master*(area_m/area_s);
                  const double s = val_s*t_base_lambda*t_base_slave;

                  for(int dd=0; dd<3; dd++){
                    if(master){
                      NN(bbr,3*bbc+dd) -= m*normal_f4[dd];
                    }else{
                      NN(bbr,3*bbc+dd) += s*normal_f4[dd];
                    }
                  }
                  ++t_base_lambda;
                }
                ++t_base_master;
                ++t_base_slave;
              }
              // cerr << "val_m  "<< val_m <<  endl;
              // cerr << "row_data.getN()  "<< row_data.getN() <<  endl;
              // cerr << "col_data.getN()  "<< col_data.getN() <<  endl;
            }
          }

        //
        // }

        Mat aij;
        if(Aij == PETSC_NULL) {
          aij = getFEMethod()->snes_B;
        } else {
          aij = Aij;
        }
        // cerr << "NN  "<< NN <<  endl;
        // cerr << "row_data.getIndices()     "<< row_data.getIndices() <<  endl;
        // cerr << "col_data.getIndices()     "<< col_data.getIndices() <<  endl;
        // string aaa;
        // cin >> aaa;
        //Assemble NN to final Aij vector based on its global indices
        ierr = MatSetValues(
          aij,
          nb_base_fun_row,&row_data.getIndices()[shift_row],
          nb_col,&col_data.getIndices()[0],
          &*NN.data().begin(),ADD_VALUES
        ); CHKERRQ(ierr);


        //Calculae and assemble trans(NN) and assemble it Aij based on its global indices
        noalias(transNN) = trans(NN);
        // cerr << "transNN  "<< transNN <<  endl;
        ierr = MatSetValues(
          aij,
          nb_col,&col_data.getIndices()[0],
          nb_base_fun_row,&row_data.getIndices()[shift_row],
          &*transNN.data().begin(),ADD_VALUES
        ); CHKERRQ(ierr);

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }
      PetscFunctionReturn(0);
    }
  };




  //Calculate CT*Lam and assemble to the Rhs  (Note OPROW here, which is LagMul here)
  struct OpCal_fcon:public FlatPrismElementForcesAndSurcesCore::UserDataOperator {

    CommonDataContact &commonDataContact;
    Vec F;
    OpCal_fcon(
      const string field_name,
      CommonDataContact &common_data_contact,
      Vec f
    ):
    FlatPrismElementForcesAndSurcesCore::UserDataOperator(
      field_name,UserDataOperator::OPCOL),
    commonDataContact(common_data_contact),
    F(f){
    }

    
    VectorDouble vec_f;
    PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;

      // cout<<" OpCal_fcon "<<endl;

      if(data.getIndices().size()==0) PetscFunctionReturn(0);

      const int nb_gauss_pts = data.getN().size1();
      int shift_col = 0;
      int nb_base_fun_col = data.getN().size2();
      if(type == MBVERTEX) {
        shift_col = 3;
        nb_base_fun_col = 3;  //total are 6 for MBVERTEX (top and bottom tris)
      }

      // const double *normal_f3_ptr = &getNormalF3()[0];
      // const double area_m = cblas_dnrm2(3,normal_f3_ptr,1)*0.5; //master tri area
      // const double *normal_f4_ptr = &getNormalF4()[0];
      // const double area_s = cblas_dnrm2(3,normal_f4_ptr,1)*0.5; //slave tri area


      //flag master used to identify master and slave edges and faces (used to assemble edges/faces as m is -ve and s is +ve)
      bool master;
      if(
        (type == MBEDGE && side <= 3)||
        (type == MBTRI && side == 3)
      ) master = true;
      else master = false;


      if(type==MBVERTEX){
        vec_f.resize(2*3*nb_base_fun_col, false); //the last false in ublas resize will destroy (not preserved) the old values
        vec_f.clear();
      }else{
        vec_f.resize(3*nb_base_fun_col, false);
        vec_f.clear();
      }

      VectorDouble normal_f4;
      for(int gg=0; gg!=nb_gauss_pts/2; gg++) {
        const double area_m = norm_2(commonDataContact.normalF3[gg])*0.5; //master tri area
        const double area_s = norm_2(commonDataContact.normalF4[gg])*0.5; //master tri area
        // cerr<<"area_m = "<< area_m << endl;
        // cerr<<"area_s = "<< area_s << endl;

        // double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*area_s;
        // double val_m = getGaussPts()(2,gg)*area_m;
        //due to integration over the parent element, do not need to multiply with jacobian (area)

        //multiply weight with 0.5 due to 1/2 in the orignial equaitons
        double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*0.5;
        double val_m = getGaussPts()(2,gg)*0.5;
        // cerr<<"val_s = "<< val_s << endl;
        // cerr<<"val_m = "<< val_m << endl;

        normal_f4=commonDataContact.normalF4[gg];

        // cerr<<"data.getN() = "<< data.getN() << endl;
        // cerr<<"normal_f4 = "<< normal_f4 << endl;
        // cerr<<"commonDataContact.lagMultAtGaussPts[gg] = "<< commonDataContact.lagMultAtGaussPts[gg] << endl;
        // cerr<<"commonDataContact.lagMultAtGaussPts[gg] = "<< commonDataContact.lagMultAtGaussPts[nb_gauss_pts/2+gg] << endl;

        FTensor::Tensor0<double*> t_base_master(&data.getN()(gg,0));
        FTensor::Tensor0<double*> t_base_slave(&data.getN()(nb_gauss_pts/2+gg,shift_col));

        if(type == MBVERTEX) {
          for(int bbc=0; bbc!=nb_base_fun_col; bbc++) {
            // cerr<<"t_base_master = "<< t_base_master << endl;
            // cerr<<"t_base_slave = "<< t_base_slave << endl << endl;
            for(int nn=0; nn!=3; nn++){
              const double m = val_m*t_base_master* normal_f4[nn] * commonDataContact.lagMultAtGaussPts[gg][0] * (area_m/area_s);
              const double s = val_s*t_base_slave * normal_f4[nn] * commonDataContact.lagMultAtGaussPts[nb_gauss_pts/2+gg][0];
              vec_f[3*bbc+nn]-=m;
              vec_f[3*nb_base_fun_col+3*bbc+nn]+=s;
            }
            ++t_base_master;
            ++t_base_slave;
          }
        }else{
          for(int bbc=0; bbc!=nb_base_fun_col; bbc++) {
            for(int nn=0; nn!=3; nn++){
              if(master){
                const double m = val_m*t_base_master * normal_f4[nn]  * commonDataContact.lagMultAtGaussPts[gg][0] * (area_m/area_s);
                vec_f(3*bbc+nn) -= m;
              } else {
                const double s = val_s*t_base_slave  * normal_f4[nn]  * commonDataContact.lagMultAtGaussPts[nb_gauss_pts/2+gg][0];
                vec_f(3*bbc+nn) += s;
              }
            }
            ++t_base_master;
            ++t_base_slave;
          }
        }//else
        // cerr<<"vec_f = "<< vec_f << endl;
        // string aaa;
        // cin>>aaa;

      }//for gauss points
      // cerr<<"nb_col = "<< data.getIndices().size() << endl;
      // cerr<<"data.getIndices() = "<< data.getIndices() << endl;
      Vec f;
      if(F == PETSC_NULL) {
        f = getFEMethod()->snes_f;
      } else {
        f = F;
      }
      const int nb_col = data.getIndices().size();
      ierr = VecSetValues(f,nb_col, &data.getIndices()[0], &vec_f[0], ADD_VALUES); CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }
  };



  //Calculate gcon and assemble to the Rhs  (Note OPROW here, which is LagMul here)
  struct OpCal_gcon:public FlatPrismElementForcesAndSurcesCore::UserDataOperator {

    CommonDataContact &commonDataContact;
    Vec F;
    OpCal_gcon(
      const string lagrang_field_name,
      CommonDataContact &common_data_contact,
      Vec f
    ):
    FlatPrismElementForcesAndSurcesCore::UserDataOperator(
      lagrang_field_name,UserDataOperator::OPROW),
    commonDataContact(common_data_contact),
    F(f){
    }

    
    VectorDouble vec_f;
    PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;
      // cout<<" OpCal_gcon "<<endl;
      if(data.getIndices().size()==0) PetscFunctionReturn(0);
      //ignore first triangle (LagMul only exists on slave (triangle 4))
      if(type == MBEDGE && side <= 3) PetscFunctionReturn(0);
      if(type == MBTRI  && side == 3)  PetscFunctionReturn(0);

      const int nb_gauss_pts = data.getN().size1();
      int shift_row = 0;
      int nb_base_fun_row = data.getN().size2();
      if(type == MBVERTEX) {
        shift_row = 3;
        nb_base_fun_row = 3;  //total are 6 for MBVERTEX (top and bottom tris)
      }
      // const double *normal_f3_ptr = &getNormalF3()[0];
      // const double area_m = cblas_dnrm2(3,normal_f3_ptr,1)*0.5; //master tri area
      // const double *normal_f4_ptr = &getNormalF4()[0];
      // const double area_s = cblas_dnrm2(3,normal_f4_ptr,1)*0.5; //slave tri area

      //as we can see all nodes (belong to both tris) but LagMul only exists on the slave sides
      vec_f.resize(nb_base_fun_row, false); //the last false in ublas resize will destroy (not preserved) the old values
      vec_f.clear();

      VectorDouble normal_f4;
      for(int gg=0; gg!=nb_gauss_pts/2; gg++) {
        const double area_m = norm_2(commonDataContact.normalF3[gg])*0.5; //master tri area
        const double area_s = norm_2(commonDataContact.normalF4[gg])*0.5; //master tri area
        // double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*area_s;
        // double val_m = getGaussPts()(2,gg)*area_m;

        //due to integration over the partent element, we don't need to multiply with jacobian (area)
        //1/2 in equaitons, so multiply with 0.5
        double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*0.5;
        double val_m = getGaussPts()(2,gg)*0.5;

        normal_f4=commonDataContact.normalF4[gg];
        // cerr<<"commonDataContact.dispAtGaussPts[gg] =                    "<< commonDataContact.dispAtGaussPts[gg] << endl;
        // cerr<<"commonDataContact.dispAtGaussPts[nb_gauss_pts/2+gg][uu] = "<< commonDataContact.dispAtGaussPts[nb_gauss_pts/2+gg] << endl;

        double nus=0; double num=0;
        for(int uu=0; uu<3; uu++){
          num+= normal_f4[uu]*commonDataContact.dispAtGaussPts[gg][uu];
          nus+= normal_f4[uu]*commonDataContact.dispAtGaussPts[nb_gauss_pts/2+gg][uu];
        }

        FTensor::Tensor0<double*> t_base_lambda(&data.getN()(nb_gauss_pts/2+gg,shift_row));
        for(int bbr=0; bbr!=nb_base_fun_row; bbr++) { //for base functions 0~3 [N1 N2 N3]
          const double m = val_m*t_base_lambda*num*(area_m/area_s);
          const double s = val_s*t_base_lambda*nus;
          vec_f[bbr]+=-m+s;
          ++t_base_lambda;
        }
      }//for gauss points

      Vec f;
      if(F == PETSC_NULL) {
        f = getFEMethod()->snes_f;
      } else {
        f = F;
      }
      // cerr<<"vec_f = "<< vec_f << endl;
      ierr = VecSetValues(f,nb_base_fun_row, &data.getIndices()[shift_row], &vec_f[0], ADD_VALUES); CHKERRQ(ierr);
      // string aaa;
      // cin>>aaa;
      PetscFunctionReturn(0);
    }
  };






    /**
      * Operator to calculate vector Fg, which will tells us that the dof is either active (+ve) or inactive (-ve)
      */
    struct OpCal_Fg:public FlatPrismElementForcesAndSurcesCore::UserDataOperator {

      CommonDataContact &commonDataContact;
      Vec Fg;
      OpCal_Fg(
        const string lagrang_field_name,
        CommonDataContact &common_data_contact,
        Vec f
      ):
      FlatPrismElementForcesAndSurcesCore::UserDataOperator(
        lagrang_field_name,UserDataOperator::OPROW),
      commonDataContact(common_data_contact),
      Fg(f){
      }

      
      VectorDouble vec_f;
      PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
        PetscFunctionBegin;
        // cout<<" OpCalC_U "<<endl;
        if(data.getIndices().size()==0) PetscFunctionReturn(0);
        //ignore first triangle (LagMul only exists on slave (triangle 4))
        if(type == MBEDGE && side <= 3) PetscFunctionReturn(0);
        if(type == MBTRI  && side == 3)  PetscFunctionReturn(0);

        const int nb_gauss_pts = data.getN().size1();
        int shift_row = 0;
        int nb_base_fun_row = data.getN().size2();
        if(type == MBVERTEX) {
          shift_row = 3;
          nb_base_fun_row = 3;  //total are 6 for MBVERTEX (top and bottom tris)
        }
        const double *normal_f3_ptr = &getNormalF3()[0];
        const double area_m = cblas_dnrm2(3,normal_f3_ptr,1)*0.5; //master tri area
        const double *normal_f4_ptr = &getNormalF4()[0];
        const double area_s = cblas_dnrm2(3,normal_f4_ptr,1)*0.5; //slave tri area

        //as we can see all nodes (belong to both tris) but LagMul only exists on the slave sides
        vec_f.resize(nb_base_fun_row, false); //the last false in ublas resize will destroy (not preserved) the old values
        vec_f.clear();

        // //unit normals to both f3 and f4
        VectorDouble n4=getNormalF4();
        VectorDouble3 n4_unit(3);
        noalias(n4_unit) = n4/norm_2(n4);
        // cerr<<"n4_unit = "<< n4_unit << endl;
        // string aaa;
        // cin>>aaa;
        for(int gg=0; gg!=nb_gauss_pts/2; gg++) {
          double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*area_s;
          double val_m = getGaussPts()(2,gg)*area_m;

          // cerr<<"commonDataContact.dispAtGaussPts[gg] =                    "<< commonDataContact.dispAtGaussPts[gg] << endl;
          // cerr<<"commonDataContact.dispAtGaussPts[nb_gauss_pts/2+gg][uu] = "<< commonDataContact.dispAtGaussPts[nb_gauss_pts/2+gg] << endl;
          // cerr<<"commonDataContact.lagMultAtGaussPts[gg] =                    "<< commonDataContact.lagMultAtGaussPts[gg] << endl;

          VectorDouble lam = commonDataContact.lagMultAtGaussPts[gg];
          VectorDouble u_m = commonDataContact.dispAtGaussPts[gg];
          VectorDouble u_s = commonDataContact.dispAtGaussPts[nb_gauss_pts/2+gg];

          //calculate nu=n.(u1 - u2)
          double nu=0;
          for(int nn=0; nn<3; nn++){
            nu+=n4_unit[nn]*(u_s[nn]-u_m[nn]);
          }
          // cerr<<"nu = "<< nu << endl;
          //calculate int N(lam- n.(u1-u2) )
          FTensor::Tensor0<double*> t_base_lambda(&data.getN()(nb_gauss_pts/2+gg,shift_row));
          for(int bbr=0; bbr!=nb_base_fun_row; bbr++) { //for base functions 0~3 [N1 N2 N3]
            const double lam_nu = val_s*t_base_lambda* (lam[0]-nu);
            vec_f[bbr]+=lam_nu;
            ++t_base_lambda;
          }
        }//for gauss points
        // cerr<<"vec_f = "<< vec_f << endl;
        // string aaa;
        // cin>>aaa;
        ierr = VecSetValues(Fg,nb_base_fun_row, &data.getIndices()[shift_row], &vec_f[0], ADD_VALUES); CHKERRQ(ierr);
        PetscFunctionReturn(0);
      }
    };



    /**
     * \biref operator to calculate and assemble Hmat for contact
     * Hmat=int ( (N * lam * dn/dx)^slave ; -(N * lam * dn/dx)^master ) dcont_surf
     */
    struct OpCalculateMatrixH:public FlatPrismElementForcesAndSurcesCore::UserDataOperator  {

      Mat Aij;
      CommonDataContact &commonDataContact;
      CommonFunctionsContact &commonFunctionsContact;
      OpCalculateMatrixH(
        const string field_name,
        CommonDataContact &common_data_contact,
        CommonFunctionsContact &common_functions_contact,
        Mat aij = PETSC_NULL
      ):
      FlatPrismElementForcesAndSurcesCore::UserDataOperator(
        field_name, field_name, UserDataOperator::OPROWCOL
      ),
      Aij(aij),
      commonDataContact(common_data_contact),
      commonFunctionsContact(common_functions_contact)
      {
        sYmm = false;  //This will make sure to loop over all entities (e.g. for order=2 it will make doWork to loop 16 time)
      }
      MatrixDouble matH;
      PetscErrorCode doWork(
        int row_side,int col_side,
        EntityType row_type,EntityType col_type,
        DataForcesAndSurcesCore::EntData &row_data,
        DataForcesAndSurcesCore::EntData &col_data
      ) {
        PetscFunctionBegin;
        try {

          //As (dn/dx)^col only exists on the slave/top side and it is for the columns, so avoide looping over master column
          if(col_type == MBEDGE && col_side < 6) PetscFunctionReturn(0);
          if(col_type == MBTRI &&  col_side == 3) PetscFunctionReturn(0);

          
          

          const int nb_row = row_data.getIndices().size();
          if(!nb_row) PetscFunctionReturn(0);
          const int nb_col = col_data.getIndices().size();
          if(!nb_col) PetscFunctionReturn(0);
          const int nb_gauss_pts = row_data.getN().size1();


          int shift_row = 0;
          int nb_base_fun_row = row_data.getN().size2();
          if(row_type == MBVERTEX) {
            shift_row = 3;
            nb_base_fun_row = 3;  //total are 6 for MBVERTEX (top and bottom tris)
          }

          int shift_col = 0;
          int nb_base_fun_col = col_data.getN().size2();
          if(col_type == MBVERTEX) {
            shift_col = 3;
            nb_base_fun_col = 3; //total are 6 for MBVERTEX (top and bottom tris)
          }
          // cerr << "nb_base_fun_row  "<< nb_base_fun_row <<  endl;
          // cerr << "nb_base_fun_col  "<< nb_base_fun_col <<  endl;

          // cerr << "3*nb_base_fun_row  "<< 3*nb_base_fun_row <<  endl;
          // cerr << "2*(3*nb_base_fun_col)  "<< 2*(3*nb_base_fun_col) <<  endl;
          //NN is matrix first half of it belong to master/bottom tri and the sencond half is blong to slave tri
          // const double *normal_f3_ptr = &getNormalF3()[0];
          // const double area_m = cblas_dnrm2(3,normal_f3_ptr,1)*0.5; //master tri area
          // const double *normal_f4_ptr = &getNormalF4()[0];
          // const double area_s = cblas_dnrm2(3,normal_f4_ptr,1)*0.5; //slave tri area
          // cerr << "area_m  "<< area_m <<  endl;
          // cerr << "area_s  "<< area_s <<  endl;


          //flag master used to identify master and slave edges and faces (used to assemble edges/faces as m is -ve and s is +ve)
          //this is for the rows as compared to the Cmat, which was for the column
          bool master;
          if(
            (row_type == MBEDGE && row_side <= 3)||
            (row_type == MBTRI && row_side == 3)
          ) master = true;
          else master = false;

          //this should be here outside the gauss points loops
          //for vertices the size of Hmat is (9x18)
          //here both rows and column are diplacments
          if(row_type == MBVERTEX) {
            //as we can see all nodes (belong to both tris) at once for MBVERTEX so size of NN is (2*3*nb_base_fun_row, 3*nb_base_fun_col)
            matH.resize(2*(3*nb_base_fun_row), 3*nb_base_fun_col,false); //the last false in ublas resize will destroy (not preserved) the old values
            matH.clear();
          }else{
            //size of NN is different than the MBVERTEX, as we cannot see entities of both(top/bottom) tris here
            matH.resize(3*nb_base_fun_row, 3*nb_base_fun_col,false);
            matH.clear();
          }
          // cerr << "matH  "<< matH <<  endl;

          //to calculate dn/dx=spin(s0).ds1/dx - spin(s1).ds0/dx  (spatial derivative of normal vector)
          MatrixDouble dn_dx_3, dn_dx_4;
          dn_dx_3.resize(3, 3*nb_base_fun_col);
          dn_dx_4.resize(3, 3*nb_base_fun_col);

          // cerr << "row_type  "<< row_type << "   row_side   "<< row_side<<"  col_type  "<< col_type << "   col_side   "<< col_side << endl;

          //loop over half of the gauss points, as we will do all caluclation (belong to both bottom/top or master/slave) in this
          for(int gg = 0;gg!=nb_gauss_pts/2;gg++) {
            const double area_m = norm_2(commonDataContact.normalF3[gg])*0.5; //master tri area
            const double area_s = norm_2(commonDataContact.normalF4[gg])*0.5; //master tri area

            // cerr << "area_m "<< area_m <<  endl;
            // cerr << "area_s "<< area_s <<  endl;
            //here integration over the parent element
            double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*0.5;
            double val_m = getGaussPts()(2,gg)*0.5;
            // cerr << "val_s "<< val_s <<  endl;
            // cerr << "val_m "<< val_m <<  endl;


            dn_dx_3.clear();
            ierr = commonFunctionsContact.calculateDn_dx(
              dn_dx_3, commonDataContact, col_type, col_data, nb_base_fun_col, gg, nb_gauss_pts,3
            ); CHKERRQ(ierr);
            // cerr << "dn_dx_3 "<<  dn_dx_3 <<  endl;

            dn_dx_4.clear();
            ierr = commonFunctionsContact.calculateDn_dx(
              dn_dx_4, commonDataContact, col_type, col_data, nb_base_fun_col, gg, nb_gauss_pts,4
            ); CHKERRQ(ierr);
            // cerr << "dn_dx_4 "<<  dn_dx_4 <<  endl<<endl;

            //calculation of d/dx(n^(1) (a2/a1))
            VectorDouble n3=commonDataContact.normalF3[gg];
            VectorDouble n4=commonDataContact.normalF4[gg];

            VectorDouble d_am_dx=(1/(4*area_m))*(prod(n3, dn_dx_3));
            VectorDouble d_as_dx=(1/(4*area_s))*(prod(n4, dn_dx_4));
            // cerr << "d_am_dx "<<  d_am_dx <<  endl;
            // cerr << "d_as_dx "<<  d_as_dx <<  endl;
            VectorDouble d_am_as= (1/(area_s*area_s)) *(area_s*d_am_dx - area_m*d_as_dx);
            // cerr << "d_am_as "<<  d_am_as <<  endl;
            // cerr << "n4 "<<  n4 <<  endl;
            // cerr << "dn_dx_4 "<<  dn_dx_4 <<  endl;
            MatrixDouble d_n_am_as = (area_m/area_s)*dn_dx_4 + outer_prod(n4, d_am_as);
            // cerr << "d_n_am_as "<<  d_n_am_as <<  endl;

            // string aaa;
            // cin >> aaa;
              // ftensor pointer pointing to the shape functions for  master and slave sides
            //(first set of guass points belong to master and the second half belong to slave)
            FTensor::Tensor0<double*> t_base_master(&row_data.getN()(gg,0));
            FTensor::Tensor0<double*> t_base_slave(&row_data.getN()(nb_gauss_pts/2+gg,shift_row));
            // cerr << "row_data.getN()  "<< row_data.getN() <<  endl;
            // cerr << "row_data.getN()  "<< row_data.getN()(gg,0) <<  endl;
            // cerr << "row_data.getN() shift_col "<< row_data.getN()(nb_gauss_pts/2+gg,shift_col) <<  endl <<  endl;
            //in this if (we will calculate and assemble both m and s to the NN as we can see all the nodes of the prism)

            double lam= commonDataContact.lagMultAtGaussPts[gg][0];
            // cerr << "lam  "<< lam <<  endl;

            // if (row_type==1 && row_side==0 && col_type==0 && col_side==0){
            //   cerr << "d_n_am_as "<<  d_n_am_as <<  endl;
            //   cerr << "row_data.getN()  "<< row_data.getN() <<  endl;
            //
            //   cerr << "lagMultAtGaussPts  "<< commonDataContact.lagMultAtGaussPts.size() <<  endl;
            //   cerr << "lam  "<< lam <<  endl;
            // }

            if(row_type == MBVERTEX) {
              for(int bbr=0; bbr!=nb_base_fun_row; bbr++) {
                const double m = val_m*t_base_master;
                const double s = val_s*t_base_slave;
                for(int ddc=0; ddc!=3; ddc++){
                  // cerr << "ddc  "<<   ddc <<  endl;
                  for(int bbc=0; bbc!=3*nb_base_fun_col; bbc++){
                    // cerr << "bbc  "<<   bbc <<  endl;
                    matH(3*bbr+ddc, bbc) -= lam*m*d_n_am_as(ddc,bbc);   //master
                    matH(3*nb_base_fun_row+3*bbr+ddc, bbc) += lam*s*dn_dx_4(ddc,bbc); //slave
                  }
                }
                ++t_base_master; //update cols master
                ++t_base_slave;  //update cols slave
              }
            }else{
              for(int bbr=0; bbr!=nb_base_fun_row; bbr++) {
                // if (row_type==1 && row_side==0 && col_type==0 && col_side==0){
                //   string aaa;
                //   cin >> aaa;
                // }
                const double m = val_m*t_base_master;
                const double s = val_s*t_base_slave;
                // if (row_type==1 && row_side==6 && col_type==0 && col_side==0){
                //   cerr << "val_m "<<  val_m <<  endl;
                //   cerr << "val_s "<<  val_s <<  endl;
                //   cerr << "t_base_master "<<  t_base_master <<  endl;
                //   cerr << "t_base_slave "<<  t_base_slave <<  endl;
                //   string aaa;
                //   cin >> aaa;
                // }
                for(int ddc=0; ddc!=3; ddc++){
                  // cerr << "ddc  "<<   ddc <<  endl;
                  for(int bbc=0; bbc!=3*nb_base_fun_col; bbc++){
                    if(master){
                      // if (row_type==1 && row_side==0 && col_type==0 && col_side==0){
                      //   cerr << "3*bbr+ddc  "<<   3*bbr+ddc << " bbc  "<<   bbc << endl;
                      // }
                      matH(3*bbr+ddc, bbc) -= lam*m*d_n_am_as(ddc,bbc);//master
                    }else{
                      matH(3*bbr+ddc, bbc) += lam*s*dn_dx_4(ddc,bbc); //slave
                    }
                  }
                }
                ++t_base_master; //update cols master
                ++t_base_slave;  //update cols slave
              }
            }//else VERTEX/EDGES

            // if (row_type==1 && row_side==0 && col_type==0 && col_side==0){
            //   string aaa;
            //   cin >> aaa;
            // }

            // cerr << "matH "<<  matH <<  endl;
            // string aaa;
            // cin>> aaa;

          } //gauss points
          // string aaa;
          // cin>> aaa;

          Mat aij;
          if(Aij == PETSC_NULL) {
            aij = getFEMethod()->snes_B;
          } else {
            aij = Aij;
          }

          //Assemble matH to final Aij vector based on its global indices
          ierr = MatSetValues(
            aij,
            nb_row,&row_data.getIndices()[0],
            3*nb_base_fun_col,&col_data.getIndices()[3*shift_col],
            &*matH.data().begin(),ADD_VALUES
          ); CHKERRQ(ierr);

        } catch (const std::exception& ex) {
          ostringstream ss;
          ss << "throw in method: " << ex.what() << endl;
          SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
        }
        PetscFunctionReturn(0);
      }
    };


    /// \biref operator to calculate and assemble Lmat for contact
    //Lmat=int ( phi (x(1)-x(2)) dn/dx ) dcont_surf
    struct OpCalculateMatrixL:public FlatPrismElementForcesAndSurcesCore::UserDataOperator  {

      Mat Aij;
      CommonDataContact &commonDataContact;
      CommonFunctionsContact &commonFunctionsContact;
      OpCalculateMatrixL(
        const string field_name,
        const string lagrang_field_name,
        CommonDataContact &common_data_contact,
        CommonFunctionsContact &common_functions_contact,
        Mat aij = PETSC_NULL
      ):
      FlatPrismElementForcesAndSurcesCore::UserDataOperator(
        lagrang_field_name, field_name, UserDataOperator::OPROWCOL
      ),
      Aij(aij),
      commonDataContact(common_data_contact),
      commonFunctionsContact(common_functions_contact)
      {
        sYmm = false;  //This will make sure to loop over all intities (e.g. for order=2 it will make doWork to loop 16 time)
      }
      MatrixDouble matL;
      PetscErrorCode doWork(
        int row_side,int col_side,
        EntityType row_type,EntityType col_type,
        DataForcesAndSurcesCore::EntData &row_data,
        DataForcesAndSurcesCore::EntData &col_data
      ) {
        PetscFunctionBegin;
        try {

          //Lagrange multipliers (rows here) only exists on slave(top surface)
          if(row_type == MBEDGE && row_side < 6) PetscFunctionReturn(0);
          if(row_type == MBTRI &&  row_side == 3) PetscFunctionReturn(0);

          //As (dn/dx)^col only exists on the slave/top side and it is for the columns, so avoide looping over master column
          if(col_type == MBEDGE && col_side < 6) PetscFunctionReturn(0);
          if(col_type == MBTRI &&  col_side == 3) PetscFunctionReturn(0);

          
          

          const int nb_row = row_data.getIndices().size();
          if(!nb_row) PetscFunctionReturn(0);
          const int nb_col = col_data.getIndices().size();
          if(!nb_col) PetscFunctionReturn(0);
          const int nb_gauss_pts = row_data.getN().size1();


          int shift_row = 0;
          int nb_base_fun_row = row_data.getN().size2();
          if(row_type == MBVERTEX) {
            shift_row = 3;
            nb_base_fun_row = 3;  //total are 6 for MBVERTEX (top and bottom tris)
          }

          int shift_col = 0;
          int nb_base_fun_col = col_data.getN().size2();
          if(col_type == MBVERTEX) {
            shift_col = 3;
            nb_base_fun_col = 3; //total are 6 for MBVERTEX (top and bottom tris)
          }
          // cerr << "nb_base_fun_row  "<< nb_base_fun_row <<  endl;
          // cerr << "nb_base_fun_col  "<< nb_base_fun_col <<  endl;

          // const double *normal_f3_ptr = &getNormalF3()[0];
          // const double area_m = cblas_dnrm2(3,normal_f3_ptr,1)*0.5; //master tri area
          // const double *normal_f4_ptr = &getNormalF4()[0];
          // const double area_s = cblas_dnrm2(3,normal_f4_ptr,1)*0.5; //slave tri area
          // cerr << "area_m  "<< area_m <<  endl;
          // cerr << "area_s  "<< area_s <<  endl;

          // //flag master used to identify master and slave edges and faces (used to assemble edges/faces as m is -ve and s is +ve)
          // //this is for the rows as compared to the Cmat, which was for the column
          // bool master;
          // if(
          //   (row_type == MBEDGE && row_side <= 3)||
          //   (row_type == MBTRI && row_side == 3)
          // ) master = true;
          // else master = false;

          //this should be here outside the gauss points loops
          //for vertices the size of Hmat is (9x18)
          //here both rows and column are diplacments

          // if(row_type == MBVERTEX) {
          //   //as we can see all nodes (belong to both tris) at once for MBVERTEX so size of NN is (2*3*nb_base_fun_row, 3*nb_base_fun_col)
          //   matH.resize(nb_base_fun_row, 3*nb_base_fun_col,false); //the last false in ublas resize will destroy (not preserved) the old values
          //   matH.clear();
          // }else{
          //   //size of NN is different than the MBVERTEX, as we cannot see entities of both(top/bottom) tris here
          //   matH.resize(nb_base_fun_row, 3*nb_base_fun_col,false);
          //   matH.clear();
          // }

          matL.resize(nb_base_fun_row, 3*nb_base_fun_col,false); //the last false in ublas resize will destroy (not preserved) the old values
          matL.clear();

          // cerr << "col_side  "<< col_side <<  endl;
          // cerr << "col_side  "<< col_side <<  endl;
          // cerr << "nb_gauss_pts  "<< nb_gauss_pts <<  endl;

          //to calculate dn/dx=spin(s0).ds1/dx - spin(s1).ds0/dx  (spatial derivative of normal vector)
          MatrixDouble dn_dx_3, dn_dx_4;
          dn_dx_3.resize(3, 3*nb_base_fun_col);
          dn_dx_4.resize(3, 3*nb_base_fun_col);

          //loop over half of the gauss points, as we will do all caluclation (belong to both bottom/top or master/slave) in this
          for(int gg = 0;gg!=nb_gauss_pts/2;gg++) {
            const double area_m = norm_2(commonDataContact.normalF3[gg])*0.5; //master tri area
            const double area_s = norm_2(commonDataContact.normalF4[gg])*0.5; //master tri area

            double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*0.5;
            double val_m = getGaussPts()(2,gg)*0.5;
            // double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*area_s;
            // double val_m = getGaussPts()(2,gg)*area_m;
            // cerr << "val_m "<< val_m <<  endl << endl;
            // cerr << "val_s "<< val_s <<  endl;

            dn_dx_3.clear();
            ierr = commonFunctionsContact.calculateDn_dx(
              dn_dx_3, commonDataContact, col_type, col_data, nb_base_fun_col, gg, nb_gauss_pts,3
            ); CHKERRQ(ierr);
            // cerr << "dn_dx_3 "<<  dn_dx_3 <<  endl;

            dn_dx_4.clear();
            ierr = commonFunctionsContact.calculateDn_dx(
              dn_dx_4, commonDataContact, col_type, col_data, nb_base_fun_col, gg, nb_gauss_pts,4
            ); CHKERRQ(ierr);
            // cerr << "dn_dx_3 "<<  dn_dx_3 <<  endl;

            //calculation of d/dx(n^(1) (a2/a1))
            VectorDouble n3=commonDataContact.normalF3[gg];
            VectorDouble n4=commonDataContact.normalF4[gg];

            VectorDouble d_am_dx=(1/(4*area_m))*(prod(n3, dn_dx_3));
            VectorDouble d_as_dx=(1/(4*area_s))*(prod(n4, dn_dx_4));
            // cerr << "d_am_dx "<<  d_am_dx <<  endl;
            // cerr << "d_as_dx "<<  d_as_dx <<  endl;
            VectorDouble d_am_as= (1/(area_s*area_s)) *(area_s*d_am_dx - area_m*d_as_dx);
            // cerr << "d_am_as "<<  d_am_as <<  endl;
            // cerr << "n4 "<<  n4 <<  endl;
            // cerr << "dn_dx_4 "<<  dn_dx_4 <<  endl;
            MatrixDouble d_n_am_as = (area_m/area_s)*dn_dx_4 + outer_prod(n4, d_am_as);
            // string aaa;
            // cin >> aaa;

            //ftensor pointer pointing to the shape functions for  master and slave sides
            //(first set of guass points belong to master and the second half belong to slave)
            // FTensor::Tensor0<double*> t_base_master(&row_data.getN()(gg,0));
            // FTensor::Tensor0<double*> t_base_slave(&row_data.getN()(nb_gauss_pts/2+gg,shift_col));
            // cerr << "col_data.getN()  "<< col_data.getN() <<  endl;
            // cerr << "col_data.getN()  "<< col_data.getN()(gg,0) <<  endl;
            // cerr << "col_data.getN() shift_col "<< col_data.getN()(nb_gauss_pts/2+gg,shift_col) <<  endl <<  endl;
            //in this if (we will calculate and assemble both m and s to the NN as we can see all the nodes of the prism)

            //gap = x^(1)-x^(2) //slave - master
            VectorDouble v_disp_m = commonDataContact.dispAtGaussPts[gg];
            VectorDouble v_disp_s = commonDataContact.dispAtGaussPts[nb_gauss_pts/2+gg];
            // cerr << "v_disp_m "<< v_disp_m<<  endl;
            // cerr << "v_disp_s "<< v_disp_s<<  endl;
            // cerr << "row_data.getN() "<< row_data.getN() <<  endl;
            // cerr << "v_gap "<< v_gap <<  endl;
            MatrixDouble m_phi_us, m_phi_um;
            m_phi_us.resize(nb_base_fun_row,3);
            m_phi_us.clear();

            m_phi_um.resize(nb_base_fun_row,3);
            m_phi_um.clear();

            FTensor::Tensor0<double*> t_base_lambda(&row_data.getN()(nb_gauss_pts/2+gg,shift_row));
            for(int bbr=0; bbr!=nb_base_fun_row; bbr++){
              for(int bbc=0; bbc!=3; bbc++){
                  //As val_m and val_s are different so we have to do this integraton separately for master and slave side
                  m_phi_um(bbr, bbc)=val_m*t_base_lambda*v_disp_m[bbc];
                  m_phi_us(bbr, bbc)=val_s*t_base_lambda*v_disp_s[bbc];
              }
              ++t_base_lambda;
            }
            // cerr << "m_phi_g "<< m_phi_g <<  endl;
            matL += prod(m_phi_us, dn_dx_4)-prod(m_phi_um, d_n_am_as);
            // cerr << "matL "<< matL <<  endl;
            // string aaa;
            // cin >> aaa;
          } //gauss points
          // cerr << "matH "<<  matH <<  endl;

          Mat aij;
          if(Aij == PETSC_NULL) {
            aij = getFEMethod()->snes_B;
          } else {
            aij = Aij;
          }

          //Assemble matH to final Aij vector based on its global indices
          ierr = MatSetValues(
            aij,
            nb_base_fun_row,&row_data.getIndices()[shift_row],
            3*nb_base_fun_col,&col_data.getIndices()[3*shift_col],
            &*matL.data().begin(),ADD_VALUES
          ); CHKERRQ(ierr);

        } catch (const std::exception& ex) {
          ostringstream ss;
          ss << "throw in method: " << ex.what() << endl;
          SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
        }
        PetscFunctionReturn(0);
      }
    };



    //for nonlinear problems
    PetscErrorCode setContactOperatorsNonlinear(
      string field_name,string lagrang_field_name,Mat aij,Vec f,
      ContactSearchKdTree::ContactCommonData_multiIndex &contact_commondata_multi_index,
      double &area_master, double &area_slave
    ) {
      PetscFunctionBegin;
      cout<<"Hi 1 from setContactOperatorsNonlinear "<<endl;
      map<int,ContactPrismsData>::iterator sit = setOfContactPrism.begin();
      for(;sit!=setOfContactPrism.end();sit++) {

        //LHS
        //calculate tangent t1 and t2 to surface f4 of the prism
        feContactLhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpGetTangent(field_name,commonDataContact));
        //calculate normal to surface f4 of the prism
        feContactLhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpGetNormal(field_name,commonDataContact));
        // calculate Cmat (lam, disp)
        feContactLhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpContactConstraintMatrixCandCT(field_name,lagrang_field_name,commonDataContact,aij));

        //calculate matrix H (disp, disp)
        //calculte LagMul at Gauss point
        feContactLhs.getOpPtrVector().push_back(new OpGetLagMulAtGaussPts(lagrang_field_name, commonDataContact));
        // for H matrix both rows and cols are field_name
        feContactLhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpCalculateMatrixH(field_name,commonDataContact,commonFunctionsContact,aij));
        //claculate matrix L (lam, disp)
        // calculte displacement/spatial position at Gauss point
        feContactLhs.getOpPtrVector().push_back(new OpGetDispAtGaussPts(field_name, commonDataContact));
        // // matrix L
        feContactLhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpCalculateMatrixL(field_name,lagrang_field_name,commonDataContact,commonFunctionsContact,aij));


        //RHS
        //calculate tangent t1 and t2 to surface f4 of the prism
        feContactRhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpGetTangent(field_name,commonDataContact));
        //calculate normal to surface f4 of the prism
        feContactRhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpGetNormal(field_name,commonDataContact));

        //calculte fcon=int(N(1)^T-N(2)^T)*Lam*n(x)
        //calculte LagMul at Gauss point
        feContactRhs.getOpPtrVector().push_back(new OpGetLagMulAtGaussPts(lagrang_field_name, commonDataContact));
        feContactRhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpCal_fcon(field_name, commonDataContact, f));

        // calculte and assemble gcon= int (phi^T * n(x) . g(x))
        // calculte displacement/spatial position at Gauss point
        feContactRhs.getOpPtrVector().push_back(new OpGetDispAtGaussPts(field_name, commonDataContact));
        feContactRhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpCal_gcon(lagrang_field_name, commonDataContact, f));

      }
      PetscFunctionReturn(0);
    }







  //setup operators for calculation of active set
  PetscErrorCode setContactOperatorsActiveSet(
    string field_name,string lagrang_field_name,Vec fg
  ) {
    PetscFunctionBegin;
    cout<<"Hi 1 from setContactOperatorsActiveSet "<<endl;
    // cout<<"setOfContactPrism[1].pRisms "<< setOfContactPrism[1].pRisms << endl;
    map<int,ContactPrismsData>::iterator sit = setOfContactPrism.begin();
    for(;sit!=setOfContactPrism.end();sit++) {
      // cout<<"Hi 2 "<<endl;
      // RHS
      // calculte displacement/spatial position at Gauss point
      feActiveSetRhs.getOpPtrVector().push_back(new OpGetDispAtGaussPts(field_name, commonDataContact));
      // //calculte LagMul at Gauss point
      feActiveSetRhs.getOpPtrVector().push_back(new OpGetLagMulAtGaussPts(lagrang_field_name, commonDataContact));

      // calculte and assemble gap function Fg= int (Nlam * Lam - n.(u1-u2))
      feActiveSetRhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpCal_Fg(lagrang_field_name, commonDataContact, fg));
    }
    PetscFunctionReturn(0);
  }



  //for linear problems
  PetscErrorCode setContactOperators(
    string field_name,string lagrang_field_name,Mat aij,Vec f,
    ContactSearchKdTree::ContactCommonData_multiIndex &contact_commondata_multi_index,
    double &area_master, double &area_slave
  ) {
    PetscFunctionBegin;
    cout<<"Hi 1 from setRVEBCsOperators "<<endl;
    cout<<"setOfContactPrism[1].pRisms "<< setOfContactPrism[1].pRisms << endl;

    map<int,ContactPrismsData>::iterator sit = setOfContactPrism.begin();
    for(;sit!=setOfContactPrism.end();sit++) {
      feContactLhs.getOpPtrVector().push_back(new ContactProblemKdTreeNormalU::OpContactConstraintMatrix(field_name,lagrang_field_name,aij));
    }
    PetscFunctionReturn(0);
  }


};
