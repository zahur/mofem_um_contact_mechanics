/* This file is part of MoFEM.
* MoFEM is free software: you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the
* Free Software Foundation, either version 3 of the License, or (at your
* option) any later version.
*
* MoFEM is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

struct ContactSearchKdTreeNonLinear: public  ContactSearchKdTree{

  ContactSearchKdTreeNonLinear(MoFEM::Interface &m_field):
  ContactSearchKdTree(m_field){
  }


  // // For nonlinear problem, i.e. finite deformation
  // //Searching for contacting triangles with the help of kdtree from MOAB
  // //fill the multi-index container (contact_commondata_multi_index) conissting of prism with corresponding integration triangles
  // //fill range_slave_master_prisms consisting of all the prisms (to be used for looping over all these finite elements)
  // PetscErrorCode contactSearchAlgorithm(Range &range_surf_master,
  //   Range &range_surf_slave,
  //   ContactCommonData_multiIndex &contact_commondata_multi_index,
  //   Range &range_slave_master_prisms) {
  //     PetscFunctionBegin;
  //
  //     //clipper library way to define one master and one slave tri and the out put will store in solution
  //     Paths path_slave(1), path_master(1), solution;
  //
  //     //it is used to convert double to integer to be used in clipper library (as the library only works for integer)
  //     //so we will convert all dimensions from double to integer and after finding common polygon will convert back to double
  //     double roundfact=1e6;
  //
  //     //Ranges of polygons in each intersections and created tris
  //     Range tris_in_polygon;
  //
  //
  //     //loop over slave triagles (to create prisms)
  //     int slave_num=0;
  //     for(Range::iterator tri_it_slave = range_surf_slave.begin(); tri_it_slave!=range_surf_slave.end();  tri_it_slave++) {
  //       slave_num++;
  //
  //       // {
  //       //   //this is just to see the output
  //       //   EntityHandle meshset_tri_slave, meshset_tri_master;
  //       //   rval = mField.get_moab().create_meshset(MESHSET_SET,meshset_tri_slave); CHKERRQ_MOAB(rval);
  //       //   rval = mField.get_moab().create_meshset(MESHSET_SET,meshset_tri_master); CHKERRQ_MOAB(rval);
  //       //
  //       //   ierr = mField.get_moab().clear_meshset(&meshset_tri_slave,1); CHKERRQ(ierr);
  //       //   rval = mField.get_moab().add_entities(meshset_tri_slave,&*tri_it_slave,1); CHKERRQ_MOAB(rval);
  //       //
  //       //   ostringstream sss;
  //       //   sss << "tri_slave_" << slave_num << ".vtk";
  //       //   rval = mField.get_moab().write_file(sss.str().c_str(),"VTK","",&meshset_tri_slave,1); CHKERRQ_MOAB(rval);
  //       // }
  //
  //       int num_nodes_slave;
  //       const EntityHandle * conn_slave = NULL;
  //       rval = mField.get_moab().get_connectivity(*tri_it_slave, conn_slave, num_nodes_slave);
  //
  //
  //       //insert prisms between master and slave triangles, who have common polygons
  //       //last three nodes belong to the slave tri and the other three belong to the master tris
  //       EntityHandle slave_master_1prism;
  //       EntityHandle prism_nodes[6];
  //       //slave is the top tri so add it to the top tri of prism [ii+3]
  //       for (int ii=0; ii<3; ii++){
  //         prism_nodes[ii+3]=conn_slave[ii];
  //       }
  //
  //       VectorDouble v_coords_slave; v_coords_slave.resize(9,false);
  //       rval = mField.get_moab().get_coords(conn_slave, 3, &*v_coords_slave.data().begin());
  //       // cerr<<"v_coords_slave = "<< v_coords_slave<<endl;
  //
  //       //rotation matrix (3x3) which will be used to transform the tri to z plane (this is the same for both slave and master tris)
  //       //so calculate it only once
  //       MatrixDouble m_rot; m_rot.resize(3,3,false);
  //       ierr=rotationMatrix(m_rot, v_coords_slave);
  //
  //       //as we need to transfer the coordinate to z=0
  //       double z_shift;
  //       VectorDouble v_coords_slave_new;  v_coords_slave_new.resize(9,false);
  //
  //       //transfer the slave tri coordinates (n=3) one by one
  //       int count=0;
  //       for(int ii=0; ii<3; ii++){
  //         // cerr<< "subrange(v_coords_old, count, count+2+1) = "<<subrange(v_coords_old, count, count+2+1)<<endl;  //subrange (vector, start, stop) excluding stop
  //         subrange(v_coords_slave_new, count, count+2+1)=prod(m_rot, subrange(v_coords_slave, count, count+2+1));
  //         count+=3;
  //       }
  //       // cerr<< "v_coords_slave_new = "<<v_coords_slave_new<<endl;
  //       z_shift=v_coords_slave_new[2];
  //
  //       count=2;
  //       for(int ii=0; ii<3; ii++){
  //         v_coords_slave_new[count]=0; count+=3;
  //       }
  //       // cerr<< "v_coords_slave_new = "<<v_coords_slave_new<<endl;
  //
  //       //multipy with roundfact to convert to integer
  //       v_coords_slave_new=roundfact*v_coords_slave_new;
  //
  //       //clear clipper vector for slave tri and fill it to be used in clipper
  //       path_slave[0].clear();
  //       path_slave[0] <<
  //       	  IntPoint(v_coords_slave_new[0],v_coords_slave_new[1]) << IntPoint(v_coords_slave_new[3],v_coords_slave_new[4]) <<
  //       	  IntPoint(v_coords_slave_new[6],v_coords_slave_new[7]);
  //
  //
  //       //find centriod of triangle based on orignial coordinates (this is created to search the nearest master tri)
  //       //it is to be used in kd-tree
  //       VectorDouble v_slave_tri_cent; v_slave_tri_cent.resize(3,false);
  //       for(int ii=0; ii<3; ii++){
  //         v_slave_tri_cent[ii]= (v_coords_slave[ii+0]+v_coords_slave[ii+3]+v_coords_slave[ii+6])/3;
  //       }
  //       // cerr<<"v_slave_tri_cent = "<< v_slave_tri_cent<<endl;
  //
  //
  //       //this reange will be filled in recursive manner, starting from the nearset one and unitill there is no more intersecting master tris
  //       Range range_closest_master_tris; range_closest_master_tris.clear();
  //       double closest_point[3];
  //       EntityHandle closest_master_tri;
  //       rval = kdTree.closest_triangle(kdTreeRootMeshset, &*v_slave_tri_cent.data().begin(),
  //              closest_point, closest_master_tri); CHKERRQ_MOAB(rval);
  //       range_closest_master_tris.insert(closest_master_tri);
  //
  //
  //       //this while loop will continue untill there is no more master tri for slave tris
  //       bool flag_first_master = true;  //for the first master, we will not find the adncenency
  //       int master_num=0;
  //       bool flag_end_of_search = false;
  //       while(flag_end_of_search == false){
  //
  //       //   {
  //       //     //this is just to save the step by step increase in range_closest_master_tris
  //       //     master_num++;
  //       //     // cout<<"while loop"<<endl;
  //       //     //this is just to see the output
  //       //     EntityHandle meshset_closest_master_tris;
  //       //     rval = mField.get_moab().create_meshset(MESHSET_SET,meshset_closest_master_tris); CHKERRQ_MOAB(rval);
  //       //     //this is just to see the output
  //       //     ierr = mField.get_moab().clear_meshset(&meshset_closest_master_tris,1); CHKERRQ(ierr);
  //       //     rval = mField.get_moab().add_entities(meshset_closest_master_tris,range_closest_master_tris); CHKERRQ_MOAB(rval);
  //       //     ostringstream sss1;
  //       //     sss1 << "slave_" << slave_num << "_master_"<< master_num << ".vtk";
  //       //     rval = mField.get_moab().write_file(sss1.str().c_str(),"VTK","",&meshset_closest_master_tris,1); CHKERRQ_MOAB(rval);
  //       //     // string aaa;
  //       //     // cin >> aaa;
  //       // }
  //
  //         Range range_master_tris_on_surf_new;
  //         if (flag_first_master == true){ //first master
  //           range_master_tris_on_surf_new=range_closest_master_tris;
  //           flag_first_master=false;
  //         } else{//the rest of master determine from adjacencies
  //            Range range_conn;
  //            rval = mField.get_moab().get_connectivity(range_closest_master_tris, range_conn); CHKERRQ_MOAB(rval);
  //           //  cerr<< "range_closest_master_tris = "<<range_closest_master_tris<<endl;
  //           //  cerr<< "range_conn = "<<range_conn<<endl;
  //
  //            Range range_adj_entities;
  //            //in the get_adjacencies, if moab::Interface::INTERSECT it will give only the tri, which is interesect of the adjacency of all nodes
  //            //moab::Interface::UNION will give al the tri, even the one which are not on the surace, so then need to interset with the master to get what we want
  //            rval = mField.get_moab().get_adjacencies(range_conn, 2, false, range_adj_entities, moab::Interface::UNION); CHKERRQ_MOAB(rval);
  //
  //            //triangel on the surface, i.e. removing the tri inside the volume
  //            Range range_master_tris_on_surf = intersect(range_surf_master, range_adj_entities);
  //
  //            //removing the tri from the range_tri_on_surf, which already exists in range_closest_master_tris
  //            range_master_tris_on_surf_new = subtract(range_master_tris_on_surf, range_closest_master_tris);
  //           //  cerr<< "range_master_tris_on_surf_new = "<<range_master_tris_on_surf_new<<endl;
  //         }
  //
  //         //looping over all master range_master_tris_on_surf_new and discart those elements, which are not in contact with master (will do it with clipper library)
  //         int num_intersections=0;
  //         for(Range::iterator tri_it_master = range_master_tris_on_surf_new.begin(); tri_it_master!=range_master_tris_on_surf_new.end();  tri_it_master++) {
  //           const EntityHandle * conn_master = NULL;
  //           int num_nodes_master = 0;
  //           rval = mField.get_moab().get_connectivity(*tri_it_master, conn_master, num_nodes_master);
  //
  //           VectorDouble v_coords_master; v_coords_master.resize(9,false);
  //           rval = mField.get_moab().get_coords(conn_master, 3, &*v_coords_master.data().begin());
  //
  //           //fill the master clipper vector
  //           VectorDouble v_coords_master_new;  v_coords_master_new.resize(9,false);
  //
  //           //rotaiton matrix for this master element is the same as for the above slave element
  //           int count=0;
  //           for(int ii=0; ii<3; ii++){
  //             subrange(v_coords_master_new, count, count+2+1)=prod(m_rot, subrange(v_coords_master, count, count+2+1));
  //             count+=3;
  //           }
  //
  //           // cerr<< "v_coords_master_new = "<<v_coords_master_new<<endl;
  //           count=2;
  //           for(int ii=0; ii<3; ii++){
  //             v_coords_master_new[count]=0; count+=3;
  //           }
  //           // cerr<< "v_coords_master_new = "<<v_coords_master_new<<endl;
  //           v_coords_master_new=roundfact*v_coords_master_new;
  //           path_master[0].clear();
  //           path_master[0] <<
  //               IntPoint(v_coords_master_new[0],v_coords_master_new[1]) << IntPoint(v_coords_master_new[3],v_coords_master_new[4]) <<
  //               IntPoint(v_coords_master_new[6],v_coords_master_new[7]);
  //
  //           //perform intersection in clipper
  //           Clipper c;
  //           c.AddPaths(path_master, ptSubject, true);
  //           c.AddPaths(path_slave, ptClip, true);
  //           c.Execute(ctIntersection, solution, pftNonZero, pftNonZero);
  //           // cerr<<" solution  = " << solution << endl;
  //           // cerr<<" solution.size()  = " << solution.size() << endl;
  //           if (solution.size()==1) { //if it intersect
  //             range_closest_master_tris.insert(*tri_it_master); //update range_closest_master_tris
  //             num_intersections+=1;
  //
  //             int n_vertices_polygon=solution[0].size();
  //             // cerr<<" n_vertices_polygon  = " << n_vertices_polygon<< endl;
  //             //the first three nodes for the prism are the master tri (insert prisms only if there is intersection between slave and master tris)
  //             for (int ii=0; ii<3; ii++){
  //               prism_nodes[ii]=conn_master[ii];
  //             }
  //
  //             //creating prism between master and slave tris
  //             rval = mField.get_moab().create_element(MBPRISM,prism_nodes,6,slave_master_1prism); CHKERRQ_MOAB(rval);
  //             range_slave_master_prisms.insert(slave_master_1prism);
  //
  //             //coordinates of intersection polygon between master and slave
  //             //here we have only coordinates of polygon, so we will need to construct vertices and polygon from it
  //             VectorDouble  v_coord_polygon;
  //             v_coord_polygon.resize(3*n_vertices_polygon); v_coord_polygon.clear();
  //
  //             //x and y coordinates of polygon (in z=0 plane)
  //             count=0;
  //             for(int ii=0; ii<n_vertices_polygon; ii++){
  //               v_coord_polygon[count]=solution[0][ii].X/roundfact;  v_coord_polygon[count+1]=solution[0][ii].Y/roundfact;
  //               count=count+3;
  //             }
  //
  //             //shift the polygon in z direction with z_shift before shifting to the plane of master and slave
  //             count=2;
  //             for(int ii=0; ii<n_vertices_polygon; ii++){
  //               v_coord_polygon[count]+=z_shift; count+=3;
  //             }
  //
  //
  //             //perform transfermation (tansfer all coordinates of polygon)
  //             VectorDouble v_coord_polygon_new;  v_coord_polygon_new.resize(3*n_vertices_polygon);
  //             count=0;
  //             for(int ii=0; ii<n_vertices_polygon; ii++){
  //               subrange(v_coord_polygon_new, count, count+2+1)=prod(trans(m_rot), subrange(v_coord_polygon, count, count+2+1));
  //               count+=3;
  //             }
  //
  //             //create moab vertices with given coordinates (from clippper) and then create polygon from these vertices
  //             std::vector<EntityHandle> conn_polygon(n_vertices_polygon);
  //             count=0;
  //             for(int ii=0; ii<n_vertices_polygon; ii++){
  //               double coord[3];
  //               coord[0]=v_coord_polygon_new[0+count];  coord[1]=v_coord_polygon_new[1+count];  coord[2]=v_coord_polygon_new[2+count];
  //               rval = mField.get_moab().create_vertex(coord, conn_polygon[ii]); CHKERRQ_MOAB(rval);
  //               count+=3;
  //             }
  //             EntityHandle polygon;
  //             rval = mField.get_moab().create_element(MBPOLYGON,&conn_polygon[0],n_vertices_polygon,polygon); CHKERRQ_MOAB(rval);
  //
  //
  //             //triangulate the polygon (if it only have 3 vertices, so it is triangle, so don't need to triangulate)
  //             tris_in_polygon.clear();
  //             if (n_vertices_polygon == 3){
  //               tris_in_polygon.insert(polygon);
  //             }else{
  //               EntityHandle new_tri_conn[3];
  //                 new_tri_conn[0]=conn_polygon[0];
  //                 for(int ii=0; ii<n_vertices_polygon-2; ii++){
  //                   new_tri_conn[1]=conn_polygon[ii+1];
  //                   new_tri_conn[2]=conn_polygon[ii+2];
  //                   //create tri in polygon
  //                   EntityHandle new_tri;
  //                   rval = mField.get_moab().create_element(MBTRI,new_tri_conn,3,new_tri); CHKERRQ_MOAB(rval);
  //                   tris_in_polygon.insert(new_tri);
  //                 }
  //             }
  //
  //             // EntityHandle meshset_tris_in_polygon;
  //             // rval = mField.get_moab().create_meshset(MESHSET_SET,meshset_tris_in_polygon); CHKERRQ_MOAB(rval);
  //             // rval = mField.get_moab().add_entities(meshset_tris_in_polygon,tris_in_polygon); CHKERRQ_MOAB(rval);
  //             // rval = mField.get_moab().write_file("tris_in_polygon.vtk","VTK","",&meshset_tris_in_polygon,1); CHKERRQ_MOAB(rval);
  //
  //             //fill the multi-index container with pair (prism between slave and master, triangles in the common polygon)
  //             contact_commondata_multi_index.insert(boost::shared_ptr<ContactCommonData>(new ContactCommonData(slave_master_1prism, tris_in_polygon)));
  //           }
  //         } //master loop for new layer arrond the previous layer
  //         // cerr<< "num_intersections = "<< num_intersections << endl;
  //         if (num_intersections==0) flag_end_of_search = true;  //this will stop further searching for the current slave and will go to next one
  //         // cerr<< "End of for loop = "<<endl;
  //         }//while loop
  //     }//slave loop
  //
  //     PetscFunctionReturn(0);
  //   }


  
};
