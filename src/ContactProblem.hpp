/* This file is part of MoFEM.
* MoFEM is free software: you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the
* Free Software Foundation, either version 3 of the License, or (at your
* option) any later version.
*
* MoFEM is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */


#ifdef __cplusplus
extern "C" {
  #endif
  #include <cblas.h>
  // #include <lapack_wrap.h>
  // #include <gm_rule.h>
  #include <quad.h>
  #ifdef __cplusplus
}
#endif


struct ContactProblem {


  struct ContactPrismsData {
    Range pRisms; // All boundary surfaces
  };
  map<int,ContactPrismsData> setOfContactPrism; ///< maps side set id with appropriate FluxData


  struct ConctactElement: public MoFEM::FlatPrismElementForcesAndSurcesCore {
    ContactProblemMultiIndex::ContactCommonData_multiIndex &contactCommondataMultiIndex;
    ConctactElement(MoFEM::Interface &mField, ContactProblemMultiIndex::ContactCommonData_multiIndex &contact_commondata_multi_index):
    MoFEM::FlatPrismElementForcesAndSurcesCore(mField), contactCommondataMultiIndex(contact_commondata_multi_index) {}
    int getRule(int order) { return -1; };


    //function to calculate area of triangle (copy from moab/CslamUtils.cpp)
    double area2D(double *a, double *b, double *c)
    {
       // (b-a)x(c-a) / 2
       return ((b[0] - a[0]) * (c[1] - a[1]) - (b[1] - a[1]) * (c[0] - a[0])) / 2;
    }


    PetscErrorCode setGaussPts(int order) {
      PetscFunctionBegin;

      order *= 2;  //multiply by 2 due to the integrand of NTN (twice the approximation)

      //Defined integration points for only 1 integrated tris
      int nb_gauss_pts_1tri;
      MatrixDouble gaussPts_1tri;
      nb_gauss_pts_1tri = QUAD_2D_TABLE[order]->npoints;
      // cerr<< "nb_gauss_pts_1tri = " << nb_gauss_pts_1tri <<endl;
      gaussPts_1tri.resize(3,nb_gauss_pts_1tri,false);
      cblas_dcopy(
        nb_gauss_pts_1tri,&QUAD_2D_TABLE[order]->points[1],3,&gaussPts_1tri(0,0),1
      );
      cblas_dcopy(
        nb_gauss_pts_1tri,&QUAD_2D_TABLE[order]->points[2],3,&gaussPts_1tri(1,0),1
      );
      cblas_dcopy(
        nb_gauss_pts_1tri,QUAD_2D_TABLE[order]->weights,1,&gaussPts_1tri(2,0),1
      );
      dataH1.dataOnEntities[MBVERTEX][0].getN(NOBASE).resize(nb_gauss_pts_1tri,3,false);
      double *shape_ptr = &*dataH1.dataOnEntities[MBVERTEX][0].getN(NOBASE).data().begin();
      cblas_dcopy(
        3*nb_gauss_pts_1tri,QUAD_2D_TABLE[order]->points,1,shape_ptr,1
      );

      //get the entity (prism in this case)
      EntityHandle common_polygon = numeredEntFiniteElementPtr->getEnt();  //we will get prism element here
      typedef ContactProblemMultiIndex::ContactCommonData_multiIndex::index<ContactProblemMultiIndex::Prism_tag>::type::iterator ItMultIndexPrism;
      ItMultIndexPrism it_mult_index_prism = contactCommondataMultiIndex.get<ContactProblemMultiIndex::Prism_tag>().find(common_polygon);

      //get integration tris of each prism
      Range range_poly_tris; range_poly_tris.clear();
      range_poly_tris=it_mult_index_prism->get()->commonIntegratedTriangle;
      // cout<<"range_poly_tris = "<<range_poly_tris<<endl;

      //Number of integration points = number of Gauss points in each common tris * no of common tris * 2
      //(2 here is due to different integratio rule of master and slave tris)
      int nb_gauss_pts = nb_gauss_pts_1tri * range_poly_tris.size() * 2;
      //gaussPts=
      //[xg1 - - - -
      //[yg1 - - - -
      //[wg1*Jac1(or area) - - - -
      gaussPts.resize(3,nb_gauss_pts,false);  //MatrixDouble gaussPts are defined in FlatPrismElementForcesAndSurcesCore
      
      
      const EntityHandle * conn_slave = NULL;
      int num_nodes_prism = 0;
      rval = mField.get_moab().get_connectivity(common_polygon, conn_slave, num_nodes_prism);
      // cout<<"num_nodes_prism = "<<num_nodes_prism<<endl;

      //for prism element bottom 3 nodes belong to slave tri and top 3 nodes belong to master tri
      VectorDouble coords_prism;  //6*3=18 coordinates for 6 nodes in 3D
      coords_prism.resize(18, false);  coords_prism.clear();
      rval = mField.get_moab().get_coords(conn_slave, num_nodes_prism, &*coords_prism.data().begin()); CHKERRQ_MOAB(rval);
      // cerr << "coords_prism = "<< coords_prism << endl;
      // cerr << "coords_prism = "<< subrange(coords_prism, 9, 18) << endl;

      VectorDouble v_elem_coords_master, v_elem_coords_slave;
      v_elem_coords_master.resize(9,false);  v_elem_coords_master.clear(); v_elem_coords_master=subrange(coords_prism, 0, 9);
      v_elem_coords_slave.resize(9,false);   v_elem_coords_slave.clear();  v_elem_coords_slave=subrange(coords_prism, 9, 18);
      // cerr<< "elem_coords_master = "<<elem_coords_master<<endl;
      // cerr<< "elem_coords_slave  = "<<elem_coords_slave<<endl;


      //Here we need to calculate the local ara of integration triangles in both master and slave surfaces
      //(we need this for weight calculation of the Gauss points)

      //To do this, first convert the master and slave trainagles to 2D case (or z=0)
      //as both master and slave triangles are oriented in 3D space

      //tansfermation(rotation) matrix (use the same matrix as defined in ContactProblemMultiIndex)
      MatrixDouble m_rot; m_rot.resize(3,3,false);
      ContactProblemMultiIndex contact_problem_mulit_index(mField);
      ierr=contact_problem_mulit_index.rotationMatrix(m_rot, v_elem_coords_master);
      // cerr << "m_rot = "<< m_rot << endl;

      VectorDouble v_elem_coords_master_new, v_elem_coords_slave_new;
      v_elem_coords_master_new.resize(9,false);  v_elem_coords_master_new.clear();
      v_elem_coords_slave_new.resize(9,false);   v_elem_coords_slave_new.clear();

      double z_shift;
      //transfer the slave and master tri coordinates (n=3) one by one to z plane
      int count=0;
      for(int ii=0; ii<3; ii++){
        subrange(v_elem_coords_master_new, count, count+2+1)=prod(m_rot, subrange(v_elem_coords_master, count, count+2+1));
        subrange(v_elem_coords_slave_new,  count, count+2+1)=prod(m_rot, subrange(v_elem_coords_slave,  count, count+2+1));
        count+=3;
      }
      // cerr<< "v_elem_coords_master_new = "<<v_elem_coords_master_new<<endl;
      // cerr<< "v_elem_coords_slave_new  = "<<v_elem_coords_slave_new<<endl;
      z_shift=v_elem_coords_master_new[2];
      count=2;
      for(int ii=0; ii<3; ii++){
        v_elem_coords_master_new[count]=0;  v_elem_coords_slave_new[count]=0; count+=3;
      }

      //master and slave tri elemnets (coord in 2D)
      double elem_coords_master[6], elem_coords_slave[6];
      elem_coords_master[0]=v_elem_coords_master_new[0];  elem_coords_master[1]=v_elem_coords_master_new[1];
      elem_coords_master[2]=v_elem_coords_master_new[3];  elem_coords_master[3]=v_elem_coords_master_new[4];
      elem_coords_master[4]=v_elem_coords_master_new[6];  elem_coords_master[5]=v_elem_coords_master_new[7];

      elem_coords_slave[0]=v_elem_coords_slave_new[0];    elem_coords_slave[1]=v_elem_coords_slave_new[1];
      elem_coords_slave[2]=v_elem_coords_slave_new[3];    elem_coords_slave[3]=v_elem_coords_slave_new[4];
      elem_coords_slave[4]=v_elem_coords_slave_new[6];    elem_coords_slave[5]=v_elem_coords_slave_new[7];
      // for (int ii=0; ii<6; ii++){
      //   cout<<"elem_coords_master = "<<elem_coords_master[ii]<<endl;
      //   cout<<"elem_coords_slave  = "<<elem_coords_slave[ii]<<endl;
      // }
      int countg=0;
      //for each prism loop over all the integration tris
      for(Range::iterator it_tri = range_poly_tris.begin(); it_tri!=range_poly_tris.end();  it_tri++) {
        const EntityHandle* conn_face;  int num_nodes_tri;
        //get nodes attached to the tri
        rval = mField.get_moab().get_connectivity(*it_tri,conn_face,num_nodes_tri,true); CHKERRQ_MOAB(rval);
        //get nodal coordinates
        VectorDouble v_coords_integration_tri;  v_coords_integration_tri.resize(9,false);  v_coords_integration_tri.clear(); //[x1 y1 z1 x2 y2 z2 .......]
        rval = mField.get_moab().get_coords(conn_face,num_nodes_tri, &*v_coords_integration_tri.data().begin()); CHKERRQ_MOAB(rval);

        //transfer coord to z plane
        VectorDouble  v_coords_integration_tri_new;
        v_coords_integration_tri_new.resize(9,false); v_coords_integration_tri_new.clear();
        int count=0;
        for(int ii=0; ii<3; ii++){
          subrange(v_coords_integration_tri_new, count, count+2+1)=prod(m_rot, subrange(v_coords_integration_tri, count, count+2+1));
          count+=3;
        }
        // cerr<< "v_coords_integration_tri_new1 = "<<v_coords_integration_tri_new<<endl;
        count=2;
        for(int ii=0; ii<3; ii++){
          v_coords_integration_tri_new[count]=0;  count+=3;
        }

        //shape function derivative for tri elements (these are constant)
        //diff_n_tri = [dN1/dxi, dN1/deta, dN2/dxi, dN2/deta, dN3/dxi, dN3/deta] = [-1 -1 1 0 0 1]
        double diff_n_tri[6];
        ierr = ShapeDiffMBTRI(diff_n_tri); CHKERRQ(ierr);
        // for (int ii=0; ii<6; ii++){
        //   cout<<"diff_n_tri = "<<diff_n_tri[ii]<<endl;
        // }
        //calculate local coordinates of each integration tri
        double n_input[3]={1, 0, 0}; //shape function at starting point
        //function to calculate the local coordinate of element based on its global coordinates
        //element local coordinates of nodes of integration tri

        double coords_integration_tri_loc_master[9], coords_integration_tri_loc_slave[9];
        count=0;
        for(int ii=0; ii<3; ii++){
          // cout<<"count = "<<count<<endl;
          double glob_coords_tri[2];
          glob_coords_tri[0]=v_coords_integration_tri_new[count];  glob_coords_tri[1]=v_coords_integration_tri_new[count+1];

          //local coordinates of integration tri in master element
          double loc_coords_tri_master[2]={0, 0}; //starting point
          ierr = ShapeMBTRI_inverse(n_input, diff_n_tri, elem_coords_master, glob_coords_tri, loc_coords_tri_master); CHKERRQ(ierr);
          coords_integration_tri_loc_master[count]=loc_coords_tri_master[0];  coords_integration_tri_loc_master[count+1]=loc_coords_tri_master[1];  coords_integration_tri_loc_master[count+2]=0.0;

          //local coordinates of integration tri in slave element
          double loc_coords_tri_slave[2]={0, 0}; //starting point
          ierr = ShapeMBTRI_inverse(n_input, diff_n_tri, elem_coords_slave, glob_coords_tri, loc_coords_tri_slave); CHKERRQ(ierr);
          coords_integration_tri_loc_slave[count]=loc_coords_tri_slave[0];  coords_integration_tri_loc_slave[count+1]=loc_coords_tri_slave[1];  coords_integration_tri_loc_slave[count+2]=0.0;

          count=count+3;
        }
        // for (int ii=0; ii<9; ii++){
        //   cout<<"coords_integration_tri_loc_master = "<<coords_integration_tri_loc_master[ii]<<endl;
        // }
        // cout<<endl;
        // for (int ii=0; ii<9; ii++){
        //   cout<<"coords_integration_tri_loc_slave = "<<coords_integration_tri_loc_slave[ii]<<endl;
        // }
       //local (not global) area or jacobian of integration tri  in both master and slave triangles (can be +ve or -ve based on surface orientation)
        double area_integration_tri_master_loc, area_integration_tri_slave_loc;
        area_integration_tri_master_loc = area2D(coords_integration_tri_loc_master, coords_integration_tri_loc_master + 3, coords_integration_tri_loc_master + 6);
        area_integration_tri_master_loc = abs(area_integration_tri_master_loc);
        // cout<<"area_integration_tri_master_loc = "<<area_integration_tri_master_loc<<endl;

        area_integration_tri_slave_loc = area2D(coords_integration_tri_loc_slave, coords_integration_tri_loc_slave + 3, coords_integration_tri_loc_slave + 6);
        area_integration_tri_slave_loc = abs(area_integration_tri_slave_loc);
        // cout<<"area_integration_tri_slave_loc = "<<area_integration_tri_slave_loc<<endl;
        // double coord_tri[9];
        // for (int ii=0; ii<9; ii++){
        //   coord_tri[ii]=0.0;
        // }
        // coord_tri[3]=1.0;  coord_tri[7]=1.0;
        // double area_try;
        // area_try = area2D(coord_tri, coord_tri + 3, coord_tri + 6);
        // cout<<"area_try "<<area_try<<endl;
        //
        //for each integration tri loop over all its Gauss points
        //calculate global coordinates of each integration point and then calculate the local coordinates
        //of this integration point in each master and slave surface
        int n1=nb_gauss_pts_1tri * range_poly_tris.size() ;
        for(int gg=0; gg<nb_gauss_pts_1tri; gg++){
          gaussPts(2,countg)   =gaussPts_1tri(2,gg)*area_integration_tri_master_loc * 2;  // 2 here is due to as for ref tri A=1/2 and w=1  or w=2*A
          gaussPts(2,countg+n1)=gaussPts_1tri(2,gg)*area_integration_tri_slave_loc  * 2;
          // cout<<"gaussPts(2,countg)    = "<<gaussPts(2,countg)<<endl;
          // cout<<"gaussPts(2,countg+n1) = "<<gaussPts(2,countg+n1)<<endl;
          // cout<<"gaussPts_1tri(2,gg) = "<<gaussPts_1tri(2,gg)<<endl;
          //shape funciton for each Guass point
          MatrixDouble N_tri;  N_tri.resize(1,3);
          ierr = ShapeMBTRI(&N_tri(0,0),&gaussPts_1tri(0,gg),&gaussPts_1tri(1,gg),1); CHKERRQ(ierr);
          // cout<<"N_tri = "<<N_tri<<endl;

          // VectorDouble v_N_tri; v_N_tri.resize(3,false); v_N_tri.clear();
          // &*v_N_tri.data().begin()=&N_tri(0,0);
          // cout<<"v_N_tri = "<<v_N_tri<<endl;

          //global coordinate of each Gauss point in 2D (z plane)
          // x and y global coordinates
          VectorDouble v_glob_coords;  v_glob_coords.resize(2,false); v_glob_coords.clear();
          // cerr<< "v_coords_integration_tri_new = "<<v_coords_integration_tri_new<<endl;
          // cerr<< "v_coords_integration_tri_new = "<<subrange(subslice(v_coords_integration_tri_new, 0, 3, 9), 0, 3)<<endl;
          // cerr<< "v_coords_integration_tri_new = "<<subrange(subslice(v_coords_integration_tri_new, 1, 3, 9), 0, 3)<<endl;
          subrange(v_glob_coords, 0, 1) = prod(N_tri, subrange(subslice(v_coords_integration_tri_new, 0, 3, 9), 0, 3));
          subrange(v_glob_coords, 1, 2) = prod(N_tri, subrange(subslice(v_coords_integration_tri_new, 1, 3, 9), 0, 3));
          // cerr<< "v_glob_coords = " << v_glob_coords <<endl;

          //calculate local coordinates of each Guass point in both slave and master tris
          double loc_coords_master[2]={0, 0}; //starting point
          double n_input[3]={1, 0, 0}; //shape function at starting point
          //function to calculate the local coordinate of element based on its global coordinates
          //master is the bottom surface
          ierr = ShapeMBTRI_inverse(n_input, diff_n_tri, elem_coords_master, &*v_glob_coords.data().begin(), loc_coords_master); CHKERRQ(ierr);
          gaussPts(0,countg)=loc_coords_master[0];
          gaussPts(1,countg)=loc_coords_master[1];
          // for (int ii=0; ii<2; ii++){
          //   cout<<"loc_coords_master = "<<loc_coords_master[ii]<<endl;
          // }
          //slave is the top surface
          double loc_coords_slave[2]={0, 0}; //starting point
          ierr = ShapeMBTRI_inverse(n_input, diff_n_tri, elem_coords_slave, &*v_glob_coords.data().begin(), loc_coords_slave); CHKERRQ(ierr);
          gaussPts(0,countg+n1)=loc_coords_slave[0];
          gaussPts(1,countg+n1)=loc_coords_slave[1];
          // for (int ii=0; ii<2; ii++){
          //   cout<<"loc_coords_slave = "<<loc_coords_slave[ii]<<endl;
          // }
          // cout<<endl;
          countg++;
          // string aaa;
          // cin >> aaa;
        }
      }
      PetscFunctionReturn(0);
    }
  };


  ConctactElement feContactRhs; //To calculate the Rhs or RVE BCs
  ConctactElement feContactLhs; //To calculate the Lhs or RVE BCs

  ConctactElement& getLoopFeContactRhs() { return feContactRhs; }
  ConctactElement& getLoopFeContactLhs() { return feContactLhs; }

  MoFEM::Interface &mField;
  ContactProblemMultiIndex::ContactCommonData_multiIndex &contactCommondataMultiIndex;
  ContactProblem(MoFEM::Interface &m_field, ContactProblemMultiIndex::ContactCommonData_multiIndex &contact_commondata_multi_index):
  mField(m_field),
  contactCommondataMultiIndex(contact_commondata_multi_index),
  feContactRhs(m_field, contact_commondata_multi_index),
  feContactLhs(m_field, contact_commondata_multi_index) {
  }


  PetscErrorCode addContactElement(
    const string element_name,
    const string field_name,
    const string lagrang_field_name,
    Range &range_slave_master_prisms
  ) {
    PetscFunctionBegin;
    
    ierr = mField.add_finite_element(element_name,MF_ZERO); CHKERRQ(ierr);
    //============================================================================================================
    //C row as Lagrange_mul and col as DISPLACEMENT
    ierr = mField.modify_finite_element_add_field_row(element_name,lagrang_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_col(element_name,field_name); CHKERRQ(ierr);
    //CT col as Lagrange_mul and row as DISPLACEMENT
    ierr = mField.modify_finite_element_add_field_col(element_name,lagrang_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_row(element_name,field_name); CHKERRQ(ierr);
    //data
    ierr = mField.modify_finite_element_add_field_data(element_name,lagrang_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_data(element_name,field_name); CHKERRQ(ierr);
    // ============================================================================================================

    setOfContactPrism[1].pRisms = range_slave_master_prisms;
    //Adding range_slave_master_prisms to Element element_name
    ierr = mField.add_ents_to_finite_element_by_type(range_slave_master_prisms,MBPRISM,element_name); CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }


  /// \biref operator to calculate and assemble Cmat for contact
  struct OpContactConstraintMatrix:public FlatPrismElementForcesAndSurcesCore::UserDataOperator  {

    Mat Aij;
    OpContactConstraintMatrix(
      const string field_name,
      const string lagrang_field_name,
      Mat aij = PETSC_NULL
    ):
    FlatPrismElementForcesAndSurcesCore::UserDataOperator(
      lagrang_field_name,field_name, UserDataOperator::OPROWCOL
    ),
    Aij(aij)
    {
      sYmm = false;  //This will make sure to loop over all intities (e.g. for order=2 it will make doWork to loop 16 time)
    }
    MatrixDouble NN,transNN;
    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSurcesCore::EntData &row_data,
      DataForcesAndSurcesCore::EntData &col_data
    ) {
      PetscFunctionBegin;
      try {
        //As lagrange multipliers are only sitting on the slave/top side
        if(row_type == MBEDGE && row_side < 6) PetscFunctionReturn(0);
        if(row_type == MBTRI && row_side == 3) PetscFunctionReturn(0);
        
        

        const int nb_row = row_data.getIndices().size();
        if(!nb_row) PetscFunctionReturn(0);
        const int nb_col = col_data.getIndices().size();
        if(!nb_col) PetscFunctionReturn(0);
        const int nb_gauss_pts = row_data.getN().size1();

        int shift_row = 0;
        int nb_base_fun_row = row_data.getN().size2();
        if(row_type == MBVERTEX) {
          shift_row = 3;
          nb_base_fun_row = 3;  //total are 6 for MBVERTEX (top and bottom tris)
        }

        int shift_col = 0;
        int nb_base_fun_col = col_data.getN().size2();
        if(col_type == MBVERTEX) {
          shift_col = 3;
          nb_base_fun_col = 3; //total are 6 for MBVERTEX (top and bottom tris)
        }
        // cerr << "nb_base_fun_row  "<< nb_base_fun_row <<  endl;
        // cerr << "nb_base_fun_col  "<< nb_base_fun_col <<  endl;
        // cerr << "3*nb_base_fun_row  "<< 3*nb_base_fun_row <<  endl;
        // cerr << "2*(3*nb_base_fun_col)  "<< 2*(3*nb_base_fun_col) <<  endl;
        //NN is matrix first half of it belong to master/bottom tri and the sencond half is blong to slave tri
        const double *normal_f3_ptr = &getNormalF3()[0];
        const double area_m = cblas_dnrm2(3,normal_f3_ptr,1)*0.5; //master tri area
        const double *normal_f4_ptr = &getNormalF4()[0];
        const double area_s = cblas_dnrm2(3,normal_f4_ptr,1)*0.5; //slave tri area
        // cerr << "area_m  "<< area_m <<  endl;
        // cerr << "area_s  "<< area_s <<  endl;

        //flag master used to identify master and slave edges and faces (used to assemble edges/faces as m is -ve and s is +ve)
        bool master;
        if(
          (col_type == MBEDGE && col_side <= 3)||
          (col_type == MBTRI && col_side == 3)
        ) master = true;
        else master = false;

        //this should be here outside the gauss points loops
        // we use NN[9x9 9x9] matrix for vertices as we can see all at once, i.e. NN[3*nb_base_fun_row, 2*(3*nb_base_fun_col)]
        // for the rest we will use NN[3*nb_base_fun_row, 3*nb_base_fun_col]
        if(col_type == MBVERTEX) {
          //as we can see all nodes (belong to both tris) at once for MBVERTEX so size of NN is (*nb_base_fun_row, 2*(3*nb_base_fun_col)
          NN.resize(3*nb_base_fun_row, 2*(3*nb_base_fun_col),false); //the last false in ublas resize will destroy (not preserved) the old values
          NN.clear();
        }else{
          //size of NN is different than the MBVERTEX, as we cannot see entities of both(top/bottom) tris here
          NN.resize(3*nb_base_fun_row, 3*nb_base_fun_col,false);
          NN.clear();
        }
        // cerr << "col_side  "<< col_side <<  endl;
        // cerr << "col_side  "<< col_side <<  endl;
        // cerr << "nb_gauss_pts  "<< nb_gauss_pts <<  endl;

        //loop over half of the gauss points, as we will do all caluclation (belong to both bottom/top or master/slave) in this
        for(int gg = 0;gg!=nb_gauss_pts/2;gg++) {
          double val_s = getGaussPts()(2,gg+nb_gauss_pts/2)*area_s;
          double val_m = getGaussPts()(2,gg)*area_m;
          // cerr << "col_data.getN() "<<  col_data.getN() <<  endl;
          // cerr << "row_data.getN()  "<< row_data.getN() <<  endl;
          // cerr << "val_s "<< val_s <<  endl;
          // cerr << "val_m "<< val_m <<  endl;

          //ftensor pointer pointing to the shape functions for  master and slave sides
          //(first set of guass points belong to master and the second half belong to slave)
          FTensor::Tensor0<double*> t_base_master(&col_data.getN()(gg,0));
          FTensor::Tensor0<double*> t_base_slave(&col_data.getN()(nb_gauss_pts/2+gg,shift_col));
          // cerr << "col_data.getN()  "<< col_data.getN()(gg,0) <<  endl;
          // cerr << "col_data.getN() shift_col "<< col_data.getN()(nb_gauss_pts/2+gg,shift_col) <<  endl;


          //in this if (we will calculate and assemble both m and s to the NN as we can see all the nodes of the prism)
          if(col_type == MBVERTEX) {
            // cerr << "NN  "<< NN <<  endl;
            for(int bbc=0; bbc!=nb_base_fun_col; bbc++) {
                FTensor::Tensor0<double*> t_base_lambda(&row_data.getN()(nb_gauss_pts/2+gg,shift_row));
                // cerr << "row_data.getN()(  "<< row_data.getN()(nb_gauss_pts/2+gg,shift_row) <<  endl;
                for(int bbr=0; bbr!=nb_base_fun_row; bbr++) {
                  const double m = val_m*t_base_lambda*t_base_master;
                  const double s = val_s*t_base_lambda*t_base_slave;
                  for(int dd = 0;dd!=3;dd++) {
                    // cerr << "3*bbr+dd  "<< 3*bbr+dd <<  endl;
                    // cerr << "3*bbc+dd  "<< 3*bbc+dd <<  endl;
                    NN(3*bbr+dd,3*bbc+dd) -= m;
                    // cerr << "3*nb_base_fun_col+3*bbc+dd  "<< 3*nb_base_fun_col+3*bbc+dd <<  endl;
                    NN(3*bbr+dd,3*nb_base_fun_col+3*bbc+dd) += s;
                  }
                  ++t_base_lambda; //update rows
                }
                ++t_base_master; //update cols master
                ++t_base_slave;  //update cols slave
              }
            }
          else {  //in the elase part we will either assemble m or s depending upon the entinity (which can belong to either master or slave tri)
            for(int bbc=0; bbc!=nb_base_fun_col; bbc++) {
                FTensor::Tensor0<double*> t_base_lambda(&row_data.getN()(nb_gauss_pts/2+gg,shift_row));
                // cerr << "t_base_master  "<< t_base_master <<  endl;
                // cerr << "row_data.getN()(  "<< row_data.getN()(nb_gauss_pts/2+gg,shift_row) <<  endl;
                for(int bbr=0; bbr!=nb_base_fun_row; bbr++) {
                  // cerr << "t_base_lambda  "<< t_base_lambda <<  endl;
                  const double m = val_m*t_base_lambda*t_base_master;
                  const double s = val_s*t_base_lambda*t_base_slave;
                  for(int dd = 0;dd!=3;dd++) {
                    // cerr << "3*bbr+dd  "<< 3*bbr+dd <<  endl;
                    // cerr << "3*bbc+dd  "<< 3*bbc+dd <<  endl;
                    // cerr << "3*nb_base_fun_col+3*bbc+dd  "<< 3*nb_base_fun_col+3*bbc+dd <<  endl;
                    if(master){
                      NN(3*bbr+dd,3*bbc+dd) -= m;
                    }else{
                      NN(3*bbr+dd,3*bbc+dd) += s;}
                    }
                  ++t_base_lambda;
                }
                ++t_base_master;
                ++t_base_slave;
              }
              // cerr << "val_m  "<< val_m <<  endl;
              // cerr << "row_data.getN()  "<< row_data.getN() <<  endl;
              // cerr << "col_data.getN()  "<< col_data.getN() <<  endl;
            }
          }

        //
        // }
        if(Aij == PETSC_NULL) {
          Aij = getFEMethod()->snes_B;
        }
        // cerr << "NN  "<< NN <<  endl;
        // cerr << "row_data.getIndices()     "<< row_data.getIndices() <<  endl;
        // cerr << "col_data.getIndices()     "<< col_data.getIndices() <<  endl;
        // string aaa;
        // cin >> aaa;

        //Assemble NN to final Aij vector based on its global indices
        ierr = MatSetValues(
          Aij,
          3*nb_base_fun_row,&row_data.getIndices()[3*shift_row],
          nb_col,&col_data.getIndices()[0],
          &*NN.data().begin(),ADD_VALUES
        ); CHKERRQ(ierr);

        //Calculae and assemble trans(NN) and assemble it Aij based on its global indices
        transNN.resize(2*(3*nb_base_fun_col),3*nb_base_fun_row,false);
        noalias(transNN) = trans(NN);
        ierr = MatSetValues(
          Aij,
          nb_col,&col_data.getIndices()[0],
          3*nb_base_fun_row,&row_data.getIndices()[3*shift_row],
          &*transNN.data().begin(),ADD_VALUES
        ); CHKERRQ(ierr);

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }
      PetscFunctionReturn(0);
    }
  };


  //for linear problems
  PetscErrorCode setContactOperators(
    string field_name,string lagrang_field_name,Mat aij,Vec f,
    ContactProblemMultiIndex::ContactCommonData_multiIndex &contact_commondata_multi_index,
    double &area_master, double &area_slave
  ) {
    PetscFunctionBegin;
    cout<<"Hi 1 from setRVEBCsOperators periodic "<<endl;
    map<int,ContactPrismsData>::iterator sit = setOfContactPrism.begin();
    for(;sit!=setOfContactPrism.end();sit++) {

      feContactLhs.getOpPtrVector().push_back(
        new OpContactConstraintMatrix(
          field_name,lagrang_field_name,aij
        )
      );

    }
    PetscFunctionReturn(0);
  }

};
