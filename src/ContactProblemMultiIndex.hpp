/* This file is part of MoFEM.
* MoFEM is free software: you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the
* Free Software Foundation, either version 3 of the License, or (at your
* option) any later version.
*
* MoFEM is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */


struct ContactProblemMultiIndex {


  MoFEM::Interface &mField;
  ContactProblemMultiIndex(MoFEM::Interface &m_field):
  mField(m_field){
  }


  //Dinitition of multi-index conatiner consisting of prisms between master and slave trias and integration_tris
  struct ContactCommonData {
    EntityHandle pRism;
    Range commonIntegratedTriangle;
    ContactCommonData(EntityHandle _prism, Range _common_integrated_triangle):pRism(_prism),
    commonIntegratedTriangle(_common_integrated_triangle) {}
  };

  struct Prism_tag {};
  struct IntegratedTriangle_tag {};

  typedef multi_index_container<
  boost::shared_ptr<ContactCommonData>,
  indexed_by<
  ordered_unique<
  tag<Prism_tag>, member<ContactCommonData,EntityHandle,&ContactCommonData::pRism> >

  // ordered_unique<
  //   tag<IntegratedTriangle_tag>, member<ContactCommonData,Range,&ContactCommonData::commonIntegratedTriangle> >
  >
  > ContactCommonData_multiIndex;




//Rotation matrix
PetscErrorCode rotationMatrix(MatrixDouble &m_rot, VectorDouble &v_coords){

  
  
  PetscFunctionBegin;
  double s1[3],s2[3],n[3];
  double diffN[6];
  ierr = ShapeDiffMBTRI(diffN); CHKERRQ(ierr);
  ierr = ShapeFaceBaseMBTRI(diffN,&*v_coords.data().begin(),n,s1,s2); CHKERRQ(ierr);

  //normalise s1 and n (s1 and n are not unit vectoor)
  double s1_mag=sqrt(s1[0]*s1[0] + s1[1]*s1[1]  + s1[2]*s1[2]);
  double n_mag= sqrt(n[0] *n[0]  + n[1] *n[1]   + n[2] *n[2]);
  for(int ii=0; ii<3; ii++){
    s1[ii]=s1[ii]/s1_mag;  n[ii]=n[ii]/n_mag;
  }

  //as s1 and s2 (from the above function) are not orthonormal,
  //we have to calculate s2, which is normal to s1 and n
  // here we use the cross procut, i.e. s2 = s1 x n
  s2[0]=s1[1]*n[2]-s1[2]*n[1];
  s2[1]=s1[2]*n[0]-s1[0]*n[2];
  s2[2]=s1[0]*n[1]-s1[1]*n[0];
  // for(int ii=0; ii<3; ii++){
  //   cerr << "s1 = "<<s1[ii]<<endl;
  // }

 //rotaiton matrix
  for(int ii=0; ii<3; ii++){
    m_rot(0,ii)=s1[ii];
    m_rot(1,ii)=s2[ii];
    m_rot(2,ii)=n[ii];
  }
  // cerr<<"mat = "<< mat <<endl;
  PetscFunctionReturn(0);
}



  //Fill multi-index container
  PetscErrorCode fillMultiIndex(Range &range_surf_master,
    Range &range_surf_slave,
    ContactCommonData_multiIndex &contact_commondata_multi_index,
    Range &range_slave_master_prisms) {

      PetscFunctionBegin;
      
      

      // //meshsets (consisting of one master, one slave and out put polygon, just used for writing vtks)
      // EntityHandle meshset_1tri_slave, meshset_1tri_master, meshset_polygon;
      // rval = mField.get_moab().create_meshset(MESHSET_SET,meshset_1tri_slave); CHKERRQ_MOAB(rval);
      // rval = mField.get_moab().create_meshset(MESHSET_SET,meshset_1tri_master); CHKERRQ_MOAB(rval);
      // rval = mField.get_moab().create_meshset(MESHSET_SET,meshset_polygon); CHKERRQ_MOAB(rval);


      //clipper library way to define one master and one slave tri and the out put will store in solution
      Paths path_slave(1), path_master(1), solution;

      //it is used to convert double to integer to be used in clipper library (as the library only works for integer)
      //so we will convert all dimensions from double to integer and after finding common polygon will convert back to double
      double roundfact=1e6;

      //Ranges of polygons in each intersections and created tris
      Range tris_in_polygon;
      //loop over slave triagles (to create prisms)
      for(Range::iterator tri_it_slave = range_surf_slave.begin(); tri_it_slave!=range_surf_slave.end();  tri_it_slave++) {
        //this is just to see the output
        // ierr = mField.get_moab().clear_meshset(&meshset_1tri_slave,1); CHKERRQ(ierr);
        // rval = mField.get_moab().add_entities(meshset_1tri_slave,&*tri_it_slave,1); CHKERRQ_MOAB(rval);

        int num_nodes_slave;
        const EntityHandle * conn_slave = NULL;
        rval = mField.get_moab().get_connectivity(*tri_it_slave, conn_slave, num_nodes_slave);

        //insert prisms between master and slave triangles, who have common polygons
        //last three nodes belong to the slave tri and the other three belong to the master tris
        EntityHandle slave_master_1prism;
        EntityHandle prism_nodes[6];
        //slave is the top tri so add it to the top tri of prism [ii+3]
        for (int ii=0; ii<3; ii++){
          prism_nodes[ii+3]=conn_slave[ii];
        }

        VectorDouble v_coords_slave; v_coords_slave.resize(9,false);
        rval = mField.get_moab().get_coords(conn_slave, 3, &*v_coords_slave.data().begin());
        // cerr<<"v_coords_slave = "<< v_coords_slave<<endl;

        //rotation matrix (3x3) which will be used to transform the tri to z plane (this is the same for both slave and master tris)
        //so calculate it only once
        MatrixDouble m_rot; m_rot.resize(3,3,false);
        ierr=rotationMatrix(m_rot, v_coords_slave);

        //as we need to transfer the coordinate to z=0
        double z_shift;
        VectorDouble v_coords_slave_new;  v_coords_slave_new.resize(9,false);

        //transfer the slave tri coordinates (n=3) one by one
        int count=0;
        for(int ii=0; ii<3; ii++){
          // cerr<< "subrange(v_coords_old, count, count+2+1) = "<<subrange(v_coords_old, count, count+2+1)<<endl;  //subrange (vector, start, stop) excluding stop
          subrange(v_coords_slave_new, count, count+2+1)=prod(m_rot, subrange(v_coords_slave, count, count+2+1));
          count+=3;
        }
        // cerr<< "v_coords_slave_new = "<<v_coords_slave_new<<endl;
        z_shift=v_coords_slave_new[2];

        count=2;
        for(int ii=0; ii<3; ii++){
          v_coords_slave_new[count]=0; count+=3;
        }
        // cerr<< "v_coords_slave_new = "<<v_coords_slave_new<<endl;

        //multipy with roundfact to convert to integer
        v_coords_slave_new=roundfact*v_coords_slave_new;

        //clear clipper vector for slave tri and fill it to be used in clipper
        path_slave[0].clear();
        path_slave[0] <<
        	  IntPoint(v_coords_slave_new[0],v_coords_slave_new[1]) << IntPoint(v_coords_slave_new[3],v_coords_slave_new[4]) <<
        	  IntPoint(v_coords_slave_new[6],v_coords_slave_new[7]);

        //loop over all master tris
        for(Range::iterator tri_it_master = range_surf_master.begin(); tri_it_master!=range_surf_master.end();  tri_it_master++) {
          // this is just to see the output
          // ierr = mField.get_moab().clear_meshset(&meshset_1tri_master,1); CHKERRQ(ierr);
          // rval = mField.get_moab().add_entities(meshset_1tri_master,&*tri_it_master,1); CHKERRQ_MOAB(rval);

          const EntityHandle * conn_master = NULL;
          int num_nodes_master = 0;
          rval = mField.get_moab().get_connectivity(*tri_it_master, conn_master, num_nodes_master);

          VectorDouble v_coords_master; v_coords_master.resize(9,false);
          rval = mField.get_moab().get_coords(conn_master, 3, &*v_coords_master.data().begin());

          //transform master tri coordinates to z=0
          //m_rot and zshift is the same as the one for slave tri, so don't need to calculate agian
          VectorDouble v_coords_master_new; v_coords_master_new.resize(9,false);
          int count=0;
          for(int ii=0; ii<3; ii++){
            subrange(v_coords_master_new, count, count+2+1)=prod(m_rot, subrange(v_coords_master, count, count+2+1));
            count+=3;
          }
          // cerr<< "v_coords_master_new = "<<v_coords_master_new<<endl;
          count=2;
          for(int ii=0; ii<3; ii++){
            v_coords_master_new[count]=0; count+=3;
          }
          // cerr<< "v_coords_master_new = "<<v_coords_master_new<<endl;

          //fill the master clipper vector
          v_coords_master_new=roundfact*v_coords_master_new;
          path_master[0].clear();
          path_master[0] <<
              IntPoint(v_coords_master_new[0],v_coords_master_new[1]) << IntPoint(v_coords_master_new[3],v_coords_master_new[4]) <<
              IntPoint(v_coords_master_new[6],v_coords_master_new[7]);

          //this is just to see the output
          // cerr<< "path_slave = "<<path_slave<<endl;
          // cerr<< "path_master = "<<path_master<<endl;
          // rval = mField.get_moab().write_file("tri_slave_1.vtk","VTK","",&meshset_1tri_slave,1); CHKERRQ_MOAB(rval);
          // rval = mField.get_moab().write_file("tri_master_1.vtk","VTK","",&meshset_1tri_master,1); CHKERRQ_MOAB(rval);

          //perform intersection in clipper
        	Clipper c;
          c.AddPaths(path_master, ptSubject, true);
          c.AddPaths(path_slave, ptClip, true);
          c.Execute(ctIntersection, solution, pftNonZero, pftNonZero);
          // cerr<<" solution  = " << solution << endl;
          // cerr<<" solution.size()  = " << solution.size() << endl;


          if(solution.size() > 0){
            int n_vertices_polygon=solution[0].size();
            // cerr<<" n_vertices_polygon  = " << n_vertices_polygon<< endl;
            //the first three nodes for the prism are the master tri (insert prisms only if there is intersection between slave and master tris)
            for (int ii=0; ii<3; ii++){
              prism_nodes[ii]=conn_master[ii];
            }
            //creating prism between master and slave tris
            rval = mField.get_moab().create_element(MBPRISM,prism_nodes,6,slave_master_1prism); CHKERRQ_MOAB(rval);
            range_slave_master_prisms.insert(slave_master_1prism);

            //coordinates of intersection polygon between master and slave
            //here we have only coordinates of polygon, so we will need to construct vertices and polygon from it
            VectorDouble  v_coord_polygon;
            v_coord_polygon.resize(3*n_vertices_polygon); v_coord_polygon.clear();

            //x and y coordinates of polygon (in z=0 plane)
            count=0;
            for(int ii=0; ii<n_vertices_polygon; ii++){
              v_coord_polygon[count]=solution[0][ii].X/roundfact;  v_coord_polygon[count+1]=solution[0][ii].Y/roundfact;
              count=count+3;
            }

            //shift the polygon in z direction with z_shift before shifting to the plane of master and slave
            count=2;
            for(int ii=0; ii<n_vertices_polygon; ii++){
              v_coord_polygon[count]+=z_shift; count+=3;
            }

            //perform transfermation (tansfer all coordinates of polygon)
            VectorDouble v_coord_polygon_new;  v_coord_polygon_new.resize(3*n_vertices_polygon);
            count=0;
            for(int ii=0; ii<n_vertices_polygon; ii++){
              subrange(v_coord_polygon_new, count, count+2+1)=prod(trans(m_rot), subrange(v_coord_polygon, count, count+2+1));
              count+=3;
            }
            // cerr<<" v_coord_polygon_new  = " << v_coord_polygon_new << endl;
            // Range range_vertices_polygon;
            // rval = mField.get_moab().create_vertices(&*v_coord_polygon_new.data().begin(), n_vertices_polygon, range_vertices_polygon); CHKERRQ_MOAB(rval);
            // cout<<"range_vertices_polygon = "<<range_vertices_polygon<<endl;

            //create moab vertices with given coordinates (from clippper) and then create polygon from these vertices
            std::vector<EntityHandle> conn_polygon(n_vertices_polygon);
            count=0;
            for(int ii=0; ii<n_vertices_polygon; ii++){
              double coord[3];
              coord[0]=v_coord_polygon_new[0+count];  coord[1]=v_coord_polygon_new[1+count];  coord[2]=v_coord_polygon_new[2+count];
              rval = mField.get_moab().create_vertex(coord, conn_polygon[ii]); CHKERRQ_MOAB(rval);
              count+=3;
            }
            EntityHandle polygon;
            rval = mField.get_moab().create_element(MBPOLYGON,&conn_polygon[0],n_vertices_polygon,polygon); CHKERRQ_MOAB(rval);

            //triangulate the polygon (if it only have 3 vertices, so it is triangle, so don't need to triangulate)
            tris_in_polygon.clear();
            if (n_vertices_polygon == 3){
              tris_in_polygon.insert(polygon);
            }else{
              EntityHandle new_tri_conn[3];
                new_tri_conn[0]=conn_polygon[0];
                for(int ii=0; ii<n_vertices_polygon-2; ii++){
                  new_tri_conn[1]=conn_polygon[ii+1];
                  new_tri_conn[2]=conn_polygon[ii+2];
                  //create tri in polygon
                  EntityHandle new_tri;
                  rval = mField.get_moab().create_element(MBTRI,new_tri_conn,3,new_tri); CHKERRQ_MOAB(rval);
                  tris_in_polygon.insert(new_tri);
                }
            }
            // EntityHandle meshset_tris_in_polygon;
            // rval = mField.get_moab().create_meshset(MESHSET_SET,meshset_tris_in_polygon); CHKERRQ_MOAB(rval);
            // rval = mField.get_moab().add_entities(meshset_tris_in_polygon,tris_in_polygon); CHKERRQ_MOAB(rval);
            // rval = mField.get_moab().write_file("tris_in_polygon.vtk","VTK","",&meshset_tris_in_polygon,1); CHKERRQ_MOAB(rval);

            //fill the multi-index container with pair (prism between slave and master, triangles in the common polygon)
            contact_commondata_multi_index.insert(boost::shared_ptr<ContactCommonData>(new ContactCommonData(slave_master_1prism, tris_in_polygon)));
          }
        } //end of master loop
    } //end of slave loop

    PetscFunctionReturn(0);
  }
};
