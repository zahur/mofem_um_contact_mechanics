/** \file arc_length_nonlinear_elasticity.cpp
 * \ingroup nonlinear_elastic_elem
 * \brief nonlinear elasticity (arc-length control)
 *
 * Solves nonlinear elastic problem. Using arc length control.
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

 static char help[] = "\
 -my_file mesh file name\n\
 -my_sr reduction of step size\n\
 -my_ms maximal number of steps\n\n";

#include <BasicFiniteElements.hpp>
using namespace MoFEM;

#include <boost/program_options.hpp>
using namespace std;
namespace po = boost::program_options;
#include <ElasticMaterials.hpp>
#include <NeoHookean.hpp>
#include <MethodForForceScaling.hpp>
#include <TimeForceScale.hpp>

#include "clipper.hpp"
using namespace ClipperLib;
#include "moab/AdaptiveKDTree.hpp"
#include "ContactSearchKdTree.hpp"
#include <ContactProblemKdTree.hpp>

int main(int argc, char *argv[]) {

  PetscInitialize(&argc,&argv,(char *)0,help);

  moab::Core mb_instance;
  moab::Interface& moab = mb_instance;
  ParallelComm* pcomm = ParallelComm::get_pcomm(&moab,MYPCOMM_INDEX);
  if(pcomm == NULL) pcomm =  new ParallelComm(&moab,PETSC_COMM_WORLD);

  PetscBool flg = PETSC_TRUE;
  char mesh_file_name[255];
  ierr = PetscOptionsGetString(PETSC_NULL,PETSC_NULL,"-my_file",mesh_file_name,255,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    SETERRQ(PETSC_COMM_SELF,1,"*** ERROR -my_file (MESH FILE NEEDED)");
  }

  PetscInt order;
  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-my_order",&order,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    order = 1;
  }

  // use this if your mesh is partitioned and you run code on parts,
  // you can solve very big problems
  PetscBool is_partitioned = PETSC_FALSE;
  ierr = PetscOptionsGetBool(PETSC_NULL,PETSC_NULL,"-my_is_partitioned",&is_partitioned,&flg); CHKERRQ(ierr);

  if(is_partitioned == PETSC_TRUE) {
    //Read mesh to MOAB
    const char *option;
    option = "PARALLEL=BCAST_DELETE;PARALLEL_RESOLVE_SHARED_ENTS;PARTITION=PARALLEL_PARTITION;";
    rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
  } else {
    const char *option;
    option = "";
    rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
  }

  MoFEM::Core core(moab);
  MoFEM::Interface& m_field = core;

  //ref meshset ref level 0
  ierr = m_field.seed_ref_level_3D(0,BitRefLevel().set(0)); CHKERRQ(ierr);
  std::vector<BitRefLevel> bit_levels;
  bit_levels.push_back(BitRefLevel().set(0));
  BitRefLevel problem_bit_level = bit_levels.back();






  Range range_surf_master, range_surf_slave;
  for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,SIDESET,it)) {
    if(it->getName().compare(0,6,"Master") == 0) {
      // cout<<" HI from range_surf_master "<<endl;
      rval = m_field.get_moab().get_entities_by_type(it->meshset,MBTRI,range_surf_master,true); CHKERRQ_MOAB(rval);
    }
  }
  cout<<"range_surf_master = "<<range_surf_master.size()<<endl;

  for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,SIDESET,it)) {
    if(it->getName().compare(0,5,"Slave") == 0) {
      // cout<<" HI from range_surf_slave "<<endl;
      rval = m_field.get_moab().get_entities_by_type(it->meshset,MBTRI,range_surf_slave,true); CHKERRQ_MOAB(rval);
    }
  }
  cout<<"range_surf_slave = "<<range_surf_slave.size()<<endl;

  EntityHandle meshset_surf_slave, meshset_surf_master;
  rval = moab.create_meshset(MESHSET_SET,meshset_surf_slave); CHKERRQ_MOAB(rval);
  rval = moab.create_meshset(MESHSET_SET,meshset_surf_master); CHKERRQ_MOAB(rval);

  rval = moab.add_entities(meshset_surf_slave,range_surf_slave); CHKERRQ_MOAB(rval);
  rval = moab.add_entities(meshset_surf_master,range_surf_master); CHKERRQ_MOAB(rval);

  rval = moab.write_mesh("surf_slave.vtk", &meshset_surf_slave, 1);
  rval = moab.write_mesh("surf_master.vtk", &meshset_surf_master, 1);


  EntityHandle meshset_tri_slave, out_put_set, meshset_polygons;
  rval = moab.create_meshset(MESHSET_SET,meshset_tri_slave); CHKERRQ_MOAB(rval); ;
  rval = moab.create_meshset(MESHSET_SET,out_put_set); CHKERRQ_MOAB(rval);
  rval = moab.create_meshset(MESHSET_SET,meshset_polygons); CHKERRQ_MOAB(rval);



  //Fields
  ierr = m_field.add_field("SPATIAL_POSITION",H1,AINSWORTH_LEGENDRE_BASE,3); CHKERRQ(ierr);
  ierr = m_field.add_field("MESH_NODE_POSITIONS",H1,AINSWORTH_LEGENDRE_BASE,3); CHKERRQ(ierr);
  ierr = m_field.add_field("LAGMULT",H1,AINSWORTH_LEGENDRE_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);

  //add entitities (by tets) to the field
  ierr = m_field.add_ents_to_field_by_type(0,MBTET,"SPATIAL_POSITION"); CHKERRQ(ierr);
  ierr = m_field.add_ents_to_field_by_type(0,MBTET,"MESH_NODE_POSITIONS"); CHKERRQ(ierr);
  ierr = m_field.add_ents_to_field_by_type(range_surf_slave,MBTRI,"LAGMULT"); CHKERRQ(ierr);

  //set app. order
  ierr = m_field.set_field_order(0,MBTET,"SPATIAL_POSITION",order); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBTRI,"SPATIAL_POSITION",order); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBEDGE,"SPATIAL_POSITION",order); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBVERTEX,"SPATIAL_POSITION",1); CHKERRQ(ierr);

  ierr = m_field.set_field_order(0,MBTET,"MESH_NODE_POSITIONS",order>1 ? 2 : 1); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBTRI,"MESH_NODE_POSITIONS",order>1 ? 2 : 1); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBEDGE,"MESH_NODE_POSITIONS",order>1 ? 2 : 1); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBVERTEX,"MESH_NODE_POSITIONS",1); CHKERRQ(ierr);

  ierr = m_field.set_field_order(0,MBTRI,"LAGMULT",order); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBEDGE,"LAGMULT",order); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBVERTEX,"LAGMULT",1); CHKERRQ(ierr);

  //build field
  ierr = m_field.build_fields(); CHKERRQ(ierr);
  {
    //10 node tets
    Projection10NodeCoordsOnField ent_method_material(m_field,"MESH_NODE_POSITIONS");
    ierr = m_field.loop_dofs("MESH_NODE_POSITIONS",ent_method_material,0); CHKERRQ(ierr);

    //initialize SPATIAL_POSITION position with MESH_NODE_POSITIONS
    ierr = m_field.set_field(0,MBVERTEX,"SPATIAL_POSITION"); CHKERRQ(ierr);
    ierr = m_field.set_field(0,MBEDGE,"SPATIAL_POSITION"); CHKERRQ(ierr);
    ierr = m_field.field_axpy(1.,"MESH_NODE_POSITIONS","SPATIAL_POSITION"); CHKERRQ(ierr);
    ierr = m_field.set_field(0,MBTRI,"SPATIAL_POSITION"); CHKERRQ(ierr);
    ierr = m_field.set_field(0,MBTET,"SPATIAL_POSITION"); CHKERRQ(ierr);
  }

  //build field
  ierr = m_field.build_fields(); CHKERRQ(ierr);

  // Add Neumann forces
  ierr = MetaNeummanForces::addNeumannBCElements(m_field,"SPATIAL_POSITION"); CHKERRQ(ierr);


  //to use hook material, i.e. linear elasticity
  // Hooke<adouble> mat_adouble;
  // Hooke<double> mat_double;

  // // to use newHookean material nonlinear elasticity
  // NeoHookean<adouble> mat_adouble;
  // NeoHookean<double> mat_double;

  // NonlinearElasticElement elastic(m_field,2);
  // ierr = elastic.setBlocks(&mat_double,&mat_adouble); CHKERRQ(ierr);


  NonlinearElasticElement elastic(m_field,2);
  ElasticMaterials elastic_materials(m_field);
  ierr = elastic_materials.setBlocks(elastic.setOfBlocks); CHKERRQ(ierr);
  ierr = elastic.addElement("ELASTIC","SPATIAL_POSITION"); CHKERRQ(ierr);
  ierr = elastic.setOperators("SPATIAL_POSITION"); CHKERRQ(ierr);


  //Range of flat prisms inserted between slave and master tris
  Range range_slave_master_prisms;
  ContactSearchKdTree contact_search_kd_tree(m_field);
  //Define multi-index conatiner to save these prisms and corresponding tris to be used for numerical integration of contact matrices
  ContactSearchKdTree::ContactCommonData_multiIndex contact_commondata_multi_index;
  //create kd_tree with master_surface only
  ierr = contact_search_kd_tree.buildTree(range_surf_master); CHKERRQ(ierr);

  //Fill this multi-index conainter contact_commondata_multi_index and range range_slave_master_prisms.
  //inputs  = [range_surf_master, range_surf_slave]
  //outputs = [contact_commondata_multi_index, range_slave_master_prisms]
  // this will be done with kd_tree search, efficient algorithm
  ierr = contact_search_kd_tree.contactSearchAlgorithm(range_surf_master, range_surf_slave, contact_commondata_multi_index, range_slave_master_prisms); CHKERRQ(ierr);
  cout<<"range_slave_master_prisms = "<<range_slave_master_prisms.size()<<endl;

  //Add these prisim (between master and slave tris) to the mofem database
  EntityHandle meshset_slave_master_prisms;
  rval = moab.create_meshset(MESHSET_SET,meshset_slave_master_prisms); CHKERRQ_MOAB(rval);
  rval = moab.add_entities(meshset_slave_master_prisms,range_slave_master_prisms); CHKERRQ_MOAB(rval);
  ierr = m_field.seed_ref_level_3D(meshset_slave_master_prisms,problem_bit_level); CHKERRQ(ierr);
  // ierr = m_field.get_entities_by_ref_level(problem_bit_level,BitRefLevel().set(),meshset_level0); CHKERRQ(ierr);


  //add contact element to be used to fill C and C^T matrices
  ContactProblemKdTree  contact_problem(m_field, contact_commondata_multi_index);
  contact_problem.addContactElement("CONTACT_ELEM","SPATIAL_POSITION","LAGMULT",range_slave_master_prisms);

  //build finite elements
  ierr = m_field.build_finite_elements(); CHKERRQ(ierr);
  //build adjacencies
  ierr = m_field.build_adjacencies(problem_bit_level); CHKERRQ(ierr);

  DMType dm_name = "ELASTIC_MECHANICS";
  ierr = DMRegister_MoFEM(dm_name); CHKERRQ(ierr);
  //craete dm instance
  DM dm;
  ierr = DMCreate(PETSC_COMM_WORLD,&dm);CHKERRQ(ierr);
  ierr = DMSetType(dm,dm_name);CHKERRQ(ierr);

  //set dm datastruture which created mofem datastructures
  ierr = DMMoFEMCreateMoFEM(dm,&m_field,dm_name,problem_bit_level); CHKERRQ(ierr);
  ierr = DMSetFromOptions(dm); CHKERRQ(ierr);
  ierr = DMMoFEMSetIsPartitioned(dm,is_partitioned); CHKERRQ(ierr);

  //add elements to dm
  ierr = DMMoFEMAddElement(dm,"ELASTIC"); CHKERRQ(ierr);
  ierr = DMMoFEMAddElement(dm,"FORCE_FE"); CHKERRQ(ierr);
  // ierr = DMMoFEMAddElement(dm,"PRESSURE_FE"); CHKERRQ(ierr);
  ierr = DMMoFEMAddElement(dm,"CONTACT_ELEM"); CHKERRQ(ierr);
  ierr = DMSetUp(dm); CHKERRQ(ierr);

  //create matrices
  Vec F,D;
  Mat Aij;
  ierr = DMCreateGlobalVector_MoFEM(dm,&D); CHKERRQ(ierr);
  ierr = VecDuplicate(D,&F); CHKERRQ(ierr);
  ierr = DMCreateMatrix_MoFEM(dm,&Aij); CHKERRQ(ierr);
  ierr = DMoFEMMeshToLocalVector(dm,D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = MatZeroEntries(Aij); CHKERRQ(ierr);


  SpatialPositionsBCFEMethodPreAndPostProc my_dirichlet_bc(m_field,"SPATIAL_POSITION",Aij,D,F);
  ierr = m_field.get_problem("ELASTIC_MECHANICS",&my_dirichlet_bc.problemPtr); CHKERRQ(ierr);
  ierr = my_dirichlet_bc.iNitalize(); CHKERRQ(ierr);
  my_dirichlet_bc.methodsOp.push_back(new TimeForceScale("-my_displacements_history",false));


  // Setting finite element method for applying tractions
  boost::ptr_map<string,NeummanForcesSurface> neumann_forces;
  TimeForceScale time_force_scale("-my_load_history",false);

  //forces on surface
  ierr = MetaNeummanForces::setMomentumFluxOperators(
    m_field,neumann_forces,PETSC_NULL,"SPATIAL_POSITION"
  ); CHKERRQ(ierr);

  for(
    boost::ptr_map<string,NeummanForcesSurface>::iterator mit = neumann_forces.begin();
    mit!=neumann_forces.end();mit++
  ) {
    mit->second->methodsOp.push_back(new TimeForceScale("-my_load_history",false));
  }



  //Adding elements to DMSnes
  //Rhs
  ierr = DMMoFEMSNESSetFunction(dm,DM_NO_ELEMENT,NULL,&my_dirichlet_bc,NULL); CHKERRQ(ierr);
  {
    boost::ptr_map<string,NeummanForcesSurface>::iterator fit;
    fit = neumann_forces.begin();
    for(;fit!=neumann_forces.end();fit++) {
      ierr = DMMoFEMSNESSetFunction(dm,fit->first.c_str(),&fit->second->getLoopFe(),NULL,NULL); CHKERRQ(ierr);
    }
  }
  double area_slave, area_master;
  area_master=0.0; area_slave=0.0;
  contact_problem.setContactOperatorsNonlinear("SPATIAL_POSITION","LAGMULT",Aij,F,contact_commondata_multi_index,area_master,area_slave);
  ierr = DMMoFEMSNESSetFunction(dm,"CONTACT_ELEM",&contact_problem.getLoopFeContactRhs(),NULL,NULL); CHKERRQ(ierr);
  ierr = DMMoFEMSNESSetFunction(dm,"ELASTIC",&elastic.getLoopFeRhs(),PETSC_NULL,PETSC_NULL); CHKERRQ(ierr);
  ierr = DMMoFEMSNESSetFunction(dm,DM_NO_ELEMENT,NULL,NULL,&my_dirichlet_bc); CHKERRQ(ierr);

  //Lhs
  ierr = DMMoFEMSNESSetJacobian(dm,DM_NO_ELEMENT,NULL,&my_dirichlet_bc,NULL); CHKERRQ(ierr);
  ierr = DMMoFEMSNESSetJacobian(dm,"ELASTIC",&elastic.getLoopFeLhs(),NULL,NULL); CHKERRQ(ierr);
  ierr = DMMoFEMSNESSetJacobian(dm,"CONTACT_ELEM",&contact_problem.getLoopFeContactLhs(),NULL,NULL); CHKERRQ(ierr);
  ierr = DMMoFEMSNESSetJacobian(dm,DM_NO_ELEMENT,NULL,NULL,&my_dirichlet_bc); CHKERRQ(ierr);



  // Create SNES solver
  SNES snes;
  SnesCtx *snes_ctx;
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes); CHKERRQ(ierr);
  //ierr = SNESSetDM(snes,dm); CHKERRQ(ierr);
  ierr = DMMoFEMGetSnesCtx(dm,&snes_ctx); CHKERRQ(ierr);
  ierr = SNESSetFunction(snes,F,SnesRhs,snes_ctx); CHKERRQ(ierr);
  ierr = SNESSetJacobian(snes,Aij,Aij,SnesMat,snes_ctx); CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes); CHKERRQ(ierr);



  //post_processing
  PostProcVolumeOnRefinedMesh post_proc(m_field);
  {
    ierr = post_proc.generateReferenceElementMesh(); CHKERRQ(ierr);
    ierr = post_proc.addFieldValuesPostProc("SPATIAL_POSITION"); CHKERRQ(ierr);
    ierr = post_proc.addFieldValuesGradientPostProc("SPATIAL_POSITION"); CHKERRQ(ierr);
  }


  double final_time = 1,delta_time = 0.1;
  ierr = PetscOptionsGetReal(0,"-my_final_time",&final_time,0); CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(0,"-my_delta_time",&delta_time,0); CHKERRQ(ierr);
  double delta_time0 = delta_time;


  //D0 is used to get the values from the previous iteration incase of divergence
  Vec D0;
  ierr = VecDuplicate(D,&D0); CHKERRQ(ierr);

  int step = 0;
  double t = 0;
  SNESConvergedReason reason = SNES_CONVERGED_ITERATING;
  for(;t<final_time;step++) {
    t += delta_time;
    PetscPrintf(PETSC_COMM_WORLD,"Step %d Time %6.4g final time %3.2g\n",step,t,final_time);

    //set time
    my_dirichlet_bc.ts_t = t;
    boost::ptr_map<string,NeummanForcesSurface>::iterator fit;
    fit = neumann_forces.begin();
    for(;fit!=neumann_forces.end();fit++) {
      fit->second->getLoopFe().ts_t = t;
    }

    ierr = VecAssemblyBegin(D);
    ierr = VecAssemblyEnd(D);
    ierr = VecCopy(D,D0); CHKERRQ(ierr);
    ierr = SNESSolve(snes,PETSC_NULL,D); CHKERRQ(ierr);

    int its;
    ierr = SNESGetIterationNumber(snes,&its); CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD,"number of Newton iterations = %D\n",its); CHKERRQ(ierr);
    ierr = SNESGetConvergedReason(snes,&reason); CHKERRQ(ierr);

    if(reason<0) { //no convergence (reduce the time step and repeat the analysis)
    // if(0) { //no convergence (reduce the time step and repeat the analysis)
      t -= delta_time;
      delta_time *= 0.1;
      ierr = VecCopy(D0,D); CHKERRQ(ierr);
    } else {
      // Adaptive algorithm for load-step
      //=============================================================================================
      const int its_d = 7;  //max iterations (if exceded will reduce the time step in the next)
      const double gamma = 0.5;
      const double max_reudction = 1;
      const double min_reduction = 1e-1;
      double reduction;
      reduction = pow((double)its_d/(double)(its+1),gamma);
      if(delta_time >= max_reudction*delta_time0 && reduction > 1) {
        reduction = 1;
      } else if(delta_time <= min_reduction*delta_time0 && reduction < 1) {
        reduction = 1;
      }
      ierr = PetscPrintf(PETSC_COMM_WORLD, "reduction delta_time = %6.4e delta_time = %6.4e\n", reduction,delta_time); CHKERRQ(ierr);
      delta_time *= reduction;
      if(reduction>1 && delta_time < min_reduction*delta_time0) {
        delta_time = min_reduction*delta_time0;
      }
      //=============================================================================================

      ierr = DMoFEMMeshToGlobalVector(dm,D,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      //Save data on mesh
      ierr = DMoFEMLoopFiniteElements(dm,"ELASTIC",&post_proc); CHKERRQ(ierr);
      string out_file_name;
      std::ostringstream stm;
      stm << "out_" << step << ".h5m";
      out_file_name = stm.str();
      ierr = PetscPrintf(PETSC_COMM_WORLD,"out file %s\n",out_file_name.c_str()); CHKERRQ(ierr);
      rval = post_proc.postProcMesh.write_file(out_file_name.c_str(),"MOAB","PARALLEL=WRITE_PART"); CHKERRQ_MOAB(rval);
    }//else for converged iteration
  }//for loop for time or load steping

  ierr = VecDestroy(&D0); CHKERRQ(ierr);
  ierr = MatDestroy(&Aij); CHKERRQ(ierr);
  ierr = VecDestroy(&F); CHKERRQ(ierr);
  ierr = VecDestroy(&D); CHKERRQ(ierr);
  ierr = PetscFinalize(); CHKERRQ(ierr);
  return 0;
}
